package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class Cluster {
public ArrayList<ClusterNode> ClusterList = new ArrayList<ClusterNode>();
//public List<ClusterSubNode> ClusterSubList = new ArrayList<ClusterSubNode>();
//int[] CSN = new int[1024*1024] ;
//public ClusterNode CList; 
//public List<ClusterSubNode> item = new ArrayList<ClusterSubNode>();

    //------------------------------------------------------------------
    //依序檢查TimeLine中所有的時間點
    //1. 如果clustertmp，就加入一個新的cluster
    //2. 把當時時間點鐘狀態為end的dose加到clustertmp中
    //3. 先檢查clustertmp是否為空的，如果是空的就加入一個新的cluster
    //   再把狀態為start的dose加到clustertmp中

    public void ConstructCluster (TimeLine timeline)
    {
        ArrayList<Integer> clustertmp; 
        int num = 0;
        int item[][] = null;
        int item_Count[] = null;
        
        //ArrayList<item> itemList;
        //List<Integer> medIdList;
        clustertmp = new ArrayList<Integer>();
        for (int i = 0; i < timeline.time_line.size(); i++)
        {
        	Log.d("Ctest", "i<" + timeline.time_line.size());
        	Log.d("Ctest", "j<" + timeline.time_line.get(i).end.size());
        	Log.d("Ctest", "j2<" + timeline.time_line.get(i).start.size());
        	
        	
        	for (int j = 0; j < timeline.time_line.get(i).end.size(); j++)
            {
        		Log.d("Ctest", "ClusterList.size() - 1 = " + (ClusterList.size() - 1));
        		
        		int IDtmp = timeline.time_line.get(i).end.get(j);
        		//ClusterNode.ClusterSubNode newsubnode = new ClusterNode.ClusterSubNode();
                //newsubnode.medID = IDtmp;
        		
        		Log.d("Ctest", "ClusterList.size() - 1 = " + (ClusterList.size() - 1) +"Size "+clustertmp.size()+" IDtmp "+IDtmp);
        		
                clustertmp.remove(clustertmp.indexOf(IDtmp));
                
                //Engine.item[ClusterList.size() - 1][Engine.item_Count[ClusterList.size() - 1]] = IDtmp;
                //item_Count[ClusterList.size() - 1]++;
                
                //ClusterList.get(ClusterList.size() - 1).additem(IDtmp);
                ClusterList.get(ClusterList.size() - 1).item.add(IDtmp);
                ClusterList.get(ClusterList.size() - 1).Dose_Count++;
                Log.d("Ctest", ".");
        		
        		
        		//2013.4.22
        		/*
        		//Log.d("Ctest", "j<"+ timeline.time_line.get(i).end.size());
        		int IDtmp = timeline.time_line.get(i).end.get(j).medID;
        		Log.d("Ctest", "IDtmp=" + IDtmp);
        		//medIdList = new ArrayList<Integer>();
                //ClusterSubNode newsubnode = new ClusterSubNode();
                //newsubnode.medID = IDtmp;
                clustertmp.remove(IDtmp);
                
                Log.d("Ctest", "size1 " + timeline.time_line.size());
                Log.d("Ctest", "" + (ClusterList.size() - 1));
                //Log.d("Ctest", "" + newsubnode.medID);
                
                //CList = ClusterList.get(ClusterList.size() - 1);
                //Log.d("Ctest", "QQ1");
                //ClusterSubList.set((ClusterList.size() - 1).item , newsubnode);
                //CSN[ClusterList.size() - 1] = IDtmp;
                //Log.d("Ctest", "QQ2");
                //ClusterList.set(ClusterList.size() - 1, CList);
                
                List<Integer> aa = new ArrayList<Integer>();
                Log.d("Ctest", ".");
                aa = item.get(ClusterList.size() - 1);
                Log.d("Ctest", "..");
                aa.add(IDtmp);
                Log.d("Ctest", "...");
                item.set(ClusterList.size() - 1, aa);
                Log.d("Ctest", "....");
                
                /*ClusterNode KK;
                Log.d("Ctest", "5566");
                KK = ClusterList.get(ClusterList.size() - 1);
                Log.d("Ctest", "55661");
                Integer A = new Integer(IDtmp);
                KK.additem(A);
                Log.d("Ctest", "55662");
                ClusterList.set(ClusterList.size() - 1, KK); //kk end (原block)
                
                Log.d("Ctest", "" + ClusterList.get(ClusterList.size() - 1).item.size());
                //ClusterList.get(ClusterList.size() - 1).additem(new Integer(IDtmp)); //此行出問題
                
                Log.d("Ctest", "size2 " + timeline.time_line.size());
                Log.d("Ctest", "" + ClusterList.get(ClusterList.size() - 1).Dose_Count);
                ClusterList.get(ClusterList.size() - 1).Dose_Count++;
                Log.d("Ctest", "" + ClusterList.get(ClusterList.size() - 1).Dose_Count);
                Log.d("Ctest", "end");*/  
        		//2013.4.22
                
            }
        	
        	for (int j = 0; j < timeline.time_line.get(i).start.size(); j++)
            {
                if (clustertmp.size() == 0)
                {
                    ClusterNode newnode;
                    newnode = new ClusterNode();
                    //newnode.setNum(num);
                    //num++;
                    ClusterList.add(newnode);
                }
                int IDtmp = timeline.time_line.get(i).start.get(j);
                clustertmp.add(IDtmp);
            }
            
            
            //Log.d("Ctest", "b" + i);
            
        }
        
        
        
        //Log.d("Ctest", "C3");
    }

//-------------------------------------------
//每個Cluster根據其中DoseList中的內容寫出Alternative的組合
//tmp作為recursive call中暫存用

    public void SetAlter(List<GroupDose> DoseList)
    {
        List<ClusterNode.AlterNode.AlterSubNode> tmp;
        tmp = new ArrayList<ClusterNode.AlterNode.AlterSubNode>();
        for (int i = 0; i < this.ClusterList.size(); i++)
            ClusterList.get(i).SetAlter(tmp, DoseList);




    }
}

/*public class Cluster {
public List<ClusterNode> ClusterList = new ArrayList<ClusterNode>();
//public List<ClusterSubNode> ClusterSubList = new ArrayList<ClusterSubNode>();
int[] CSN = new int[1024*1024] ;
public ClusterNode CList;    

    //------------------------------------------------------------------
    //依序檢查TimeLine中所有的時間點
    //1. 如果clustertmp，就加入一個新的cluster
    //2. 把當時時間點鐘狀態為end的dose加到clustertmp中
    //3. 先檢查clustertmp是否為空的，如果是空的就加入一個新的cluster
    //   再把狀態為start的dose加到clustertmp中

    public void ConstructCluster (TimeLine timeline)
    {
        List<Integer> clustertmp;            
        clustertmp = new ArrayList<Integer>();
        for (int i = 0; i < timeline.time_line.size(); i++)
        {
        	Log.d("Ctest", "i<" + timeline.time_line.size());
        	Log.d("Ctest", "j<" + timeline.time_line.get(i).end.size());
        	
        	for (int j = 0; j < timeline.time_line.get(i).end.size(); j++)
            {
        		//Log.d("Ctest", "j<"+ timeline.time_line.get(i).end.size());
        		int IDtmp = timeline.time_line.get(i).end.get(j).medID;
        		Log.d("Ctest", "IDtmp=" + IDtmp);
                ClusterSubNode newsubnode = new ClusterSubNode();
                newsubnode.medID = IDtmp;
                clustertmp.remove(IDtmp);
                
                Log.d("Ctest", "size1 " + timeline.time_line.size());
                Log.d("Ctest", "" + (ClusterList.size() - 1));
                Log.d("Ctest", "" + newsubnode.medID);
                
                //CList = ClusterList.get(ClusterList.size() - 1);
                //Log.d("Ctest", "QQ1");
                //ClusterSubList.set(ClusterList.size() - 1, newsubnode);
                CSN[ClusterList.size() - 1] = IDtmp;
                //Log.d("Ctest", "QQ2");
                //ClusterList.set(ClusterList.size() - 1, CList);
                
                //ClusterList.get(ClusterList.size() - 1).item.add(newsubnode); //此行出問題
                
                Log.d("Ctest", "size2 " + timeline.time_line.size());
                ClusterList.get(ClusterList.size() - 1).Dose_Count++;
                Log.d("Ctest", "end");
            }
            
            for (int j = 0; j < timeline.time_line.get(i).start.size(); j++)
            {
                if (clustertmp.size() == 0)
                {
                    ClusterNode newnode;
                    newnode = new ClusterNode();
                    ClusterList.add(newnode);
                }
                int IDtmp = timeline.time_line.get(i).start.get(j).medID;
                clustertmp.add(IDtmp);
            }
            //Log.d("Ctest", "b" + i);
            
        }
        //Log.d("Ctest", "C3");
    }

//-------------------------------------------
//每個Cluster根據其中DoseList中的內容寫出Alternative的組合
//tmp作為recursive call中暫存用

    public void SetAlter(List<GroupDose> DoseList)
    {
        List<ClusterNode.AlterNode.AlterSubNode> tmp;
        tmp = new ArrayList<ClusterNode.AlterNode.AlterSubNode>();
        for (int i = 0; i < this.ClusterList.size(); i++)
            ClusterList.get(i).SetAlter(tmp, DoseList);




    }
}*/
