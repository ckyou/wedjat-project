package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.Input.Importance;
import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;
import Jama.EigenvalueDecomposition;
import Jama.Matrix;
import android.util.Log;
//import tw.edu.nctu.cs.pet.Wedjat.Group.ClusterNode.ClusterSubNode;

	/*class ClusterSubNode //static執行成功需確認
    {
        public int medID;		//紀錄Cluster所含的dose在Dose list中的位址
    }*/
public class ClusterNode {
	
	public ArrayList<Integer> item = new ArrayList<Integer>();
	
	/*public class ClusterSubNode
    {
        public int medID;		//紀錄Cluster所含的dose在Dose list中的位址
    }*/


    static class AlterNode		//一個AlterNode中紀錄一種組合方式
    {
        public ArrayList<AlterSubNode> Alter_SubList = new ArrayList<AlterSubNode>();
        public int GroupNumber;
        public double score;
        public ArrayList<Double> factor;
        

        public void ComputeFactor(ArrayList<GroupDose> DoseList,TimeSlot TimeSlot)
        {
            factor=new ArrayList<Double>();
            factor.add(new Double(1.0/(double)this.GroupNumber));
            Log.d("QOO", "" + this.GroupNumber);
            Log.d("QOO", "   " + 1.0/(double)this.GroupNumber);
            Log.d("QOO", "      " + factor.get(factor.size()-1));
            for (int i = 0; i < Alter_SubList.size(); ++i)
            {
            	//factor.Add(TimeSlot.TimeSlotList[Alter_SubList[i].TimeSlotPosition].End_Time - TimeSlot.TimeSlotList[Alter_SubList[i].TimeSlotPosition].Start_Time);
                //factor.Add(DoseList[Alter_SubList[i].DoseID].SubDoseList.Count - Alter_SubList[i].SubDosePosition);
                factor.add((double) (TimeSlot.TimeSlotList.get(Alter_SubList.get(i).TimeSlotPosition).End_Time - TimeSlot.TimeSlotList.get(Alter_SubList.get(i).TimeSlotPosition).Start_Time));
                //Log.d("QOO", "" + (double) (TimeSlot.TimeSlotList.get(Alter_SubList.get(i).TimeSlotPosition).End_Time - TimeSlot.TimeSlotList.get(Alter_SubList.get(i).TimeSlotPosition).Start_Time));
                factor.add((double) (DoseList.get(Alter_SubList.get(i).DoseID).SubDoseList.size() - Alter_SubList.get(i).SubDosePosition));
            }
            for(int i=0;i<factor.size();i++){
            	Log.d("QOO",factor.get(i)+", ");
            }
            
        }
        
        public AlterNode()
        {
            score=0.0;
        }

        
        static class AlterSubNode	//AlterSubNode紀錄一個SubDose的資訊
        {
            public int DoseID;
            public int SubDosePosition;
            public int TimeSlotPosition;
            public int start;
            public int end;
            public MedicationDirection medicine;
            public AlterSubNode(int dose, int subdose,int timsloposition)
            {
                DoseID = dose;
                SubDosePosition = subdose;
                TimeSlotPosition = timsloposition;
            }
        }
    }

    public int Dose_Count;					//Cluster中擁有的dose數量
    public ArrayList<AlterNode> Alter_List = new ArrayList<AlterNode>();		//Cluster中的所有Dose所能形成的服藥組合
    //public ArrayList<ClusterSubNode> item;		//Cluster的dose list
    //public ArrayList<Integer> item;
    //public int item.get(i) = 0;
    //public int[] item = {0};
    //item = new ArrayList<Integer>();
    
    public int num;
    
    public void setNum(int n){
    	this.num = n;
    }
    
    /*public void additem(int n){
    	this.item[item_Count]=n;
    	this.item_Count++;
    }*/
    
    public void ComputeScore()
    {
        for (int i = 0; i < Alter_List.size(); ++i)
        {
            for (int j = 0; j < Alter_List.get(i).factor.size(); ++j)
            {
                Alter_List.get(i).score +=( Alter_List.get(i).factor.get(j) * weight.get(j));
            
            }
           
        }
    }

    public ArrayList<Double> weight=new ArrayList<Double> ();
	//public Object ClusterSubNode;
    public void ComputeWeight(List<GroupDose> DoseList)
    {
        
        /*
         * Group and Dose
         */
        //Imatrix WGD = this.ComputeWGD();
    	Matrix WGD =this.ComputeWGD();

        //this.weight.Add(WGD[0, 0]);
    	this.weight.add(WGD.get(0,0));

        Matrix WDD = this.ComputeWDD(WGD,DoseList);


        for (int i = 0; i < item.size(); ++i)
        {
            Matrix WD = ComputeWD(i, WDD, DoseList);

            this.weight.add(WD.get(0, 0));
            this.weight.add(WD.get(1, 0));
        }

    }


    private Matrix ComputeWDD(Matrix WGD, List<GroupDose> DoseList)
    {
    	double[][] E = new double[this.item.size()][this.item.size()];
    	Matrix ADD = new Matrix(E);
        //Matrix ADD = new Matrix(E, this.item.size(), this.item.size());
        for (int i = 0; i < this.item.size(); ++i)
        {
            for (int j = 0; j < this.item.size(); ++j)
            {
                ADD.set(i, j, ComputeRation(i, j, DoseList)); 
            }
        }
        //EigenvalueDecomposition eigenADD =  new EigenvalueDecomposition(ADD).GetEigenvalueDecomposition();
        int tmp = 0;
        double[][] F = new double[this.item.size()][1];
        Matrix WDD = new Matrix(F);
        //Matrix WDD = new Matrix(F, this.item.size(), 1);
        double total = 0.0;
        Matrix eigenvectorADD = new EigenvalueDecomposition(ADD).getV();
        for (double eigenvalue : new EigenvalueDecomposition(ADD).getRealEigenvalues())
        {
            if (Math.abs(eigenvalue - (double)ADD.getColumnDimension()) < 0.1)  //改用相除的比例
            {
                for (int i = 0; i < ADD.getColumnDimension(); ++i)
                {
                	WDD.set(i, 0, eigenvectorADD.get(i, tmp));
                	total += eigenvectorADD.get(i, tmp);
                }
            }
           
            ++tmp;
        }
        if (total == 0.0) System.out.println("Weight Eigenvalue is wrong!");
        for (int i = 0; i < ADD.getColumnDimension(); ++i)
        {
            WDD.set(i, 0, WDD.get(i, 0) * WGD.get(1, 0) / total);
        }

        return WDD;
    }
    private double ComputeRation(int i, int j,List<GroupDose> DoseList)
    {
        if (DoseList.get(this.item.get(i)).medicine.type == Importance.IMPORTANT){
            if (DoseList.get(this.item.get(j)).medicine.type == Importance.ORDINARY)
            {
                return (9.0 / 5.0);
            }
            else if (DoseList.get(this.item.get(j)).medicine.type == Importance.NONNECESSARY)
            {
                return (9.0 / 3.0);
            }
            else{
                return 1.0;
            }
        }
        else if (DoseList.get(this.item.get(i)).medicine.type == Importance.ORDINARY)
        {
            if (DoseList.get(this.item.get(j)).medicine.type == Importance.IMPORTANT)
            {
                return (5.0 / 9.0);
            }
            else if (DoseList.get(this.item.get(j)).medicine.type == Importance.NONNECESSARY)
            {
                return (9.0 / 5.0);
            }
            else{
                return 1.0;
            }
        }
        else if (DoseList.get(this.item.get(i)).medicine.type == Importance.NONNECESSARY)
        {
            if (DoseList.get(this.item.get(j)).medicine.type == Importance.IMPORTANT)
            {
                return (3.0 / 9.0);
            }
            else if (DoseList.get(this.item.get(j)).medicine.type == Importance.ORDINARY)
            {
                return (3.0 / 5.0);
            }
            else { 
              return 1.0;
            }
        }
        else{
            return 1.0;
        }

    }

    private Matrix ComputeWGD()
    {
    	double[][] A = new double[2][2];
    	Matrix AGD = new Matrix(A);
        //Matrix AGD = new Matrix(A, 2, 2);
        AGD.set(0, 0, 1.0); AGD.set(0, 1, 1.0);
        AGD.set(1, 0, 1.0); AGD.set(1, 1, 1.0);
        //EigenvalueDecomposition eigenAGD = AGD.GetEigenvalueDecomposition();
        int tmp = 0;
        double[][] B = new double[2][1];
        Matrix WGD = new Matrix(B);
        //Matrix WGD = new Matrix(B, 2, 1);
        double total = 0.0;
        Matrix eigenvectorAGD = new EigenvalueDecomposition(AGD).getV();
        for (double eigenvalue : new EigenvalueDecomposition(AGD).getRealEigenvalues())
        {
            if (Math.abs(eigenvalue - (double)AGD.getColumnDimension()) < 0.1)
            {
                for (int i = 0; i < AGD.getColumnDimension(); ++i)
                {
                    WGD.set(i, 0, eigenvectorAGD.get(i, tmp));
                    total += WGD.get(i, 0);
                    //Console.WriteLine(eigenA[i, tmp]);
                }
            }
            
            ++tmp;
        }
        if (total == 0.0) System.out.println("Weight Eigenvalue is wrong!");
        for (int i = 0; i < AGD.getColumnDimension(); ++i)
        {
            WGD.set(i, 0, WGD.get(i, 0) / total);
        }
        return WGD;
    }

    private Matrix ComputeWD(int  i,Matrix WDD, List<GroupDose> DoseList)
    {
    	
    	double[][] C = new double[2][2];
    	Matrix AD = new Matrix(C);
        //Matrix AD = new Matrix(C, 2, 2);
        if (DoseList.get(item.get(i)).medicine.type == Importance.IMPORTANT)
        {
            AD.set(0, 0, 1.0); AD.set(0, 1, 7.0/9.0);
            AD.set(1, 0, 9.0/7.0); AD.set(1, 1, 1.0);
        }
        else if (DoseList.get(item.get(i)).medicine.type == Importance.ORDINARY)
        {
            AD.set(0, 0, 1.0); AD.set(0, 1, 9.0/7.0);
            AD.set(1, 0, 7.0/9.0); AD.set(1, 1, 1.0);
        }
        else if (DoseList.get(item.get(i)).medicine.type == Importance.NONNECESSARY)
        {
            AD.set(0, 0, 1.0); AD.set(0, 1, 1.0);
            AD.set(1, 0, 1.0); AD.set(1, 1, 1.0);
        }
        //EigenvalueDecomposition eigenAD = AD.GetEigenvalueDecomposition();
        int tmp = 0;
        double[][] D = new double[2][1];
        Matrix WD = new Matrix(D);
        //Matrix WD = new Matrix(D, 2, 1);
        double total = 0.0;
        Matrix eigenvectorAD = new EigenvalueDecomposition(AD).getV();
        for (double eigenvalue : new EigenvalueDecomposition(AD).getRealEigenvalues())
        {

            //Console.WriteLine("eig "+eigenvalue);
            //Console.WriteLine("AD.Column " + (double)AD.Columns);
            if (Math.abs(eigenvalue - (double)AD.getColumnDimension()) < 0.1)
            {
                for (int k = 0; k < AD.getColumnDimension(); ++k)
                {
                    WD.set(k, 0, eigenvectorAD.get(k, tmp));
                    total += WD.get(k, 0);
                    //Console.WriteLine(eigenA[i, tmp]);
                }
            }

            ++tmp;
        }
        if (total == 0.0) System.out.println("Weight Eigenvalue is wrong!");
        //Console.WriteLine(total);
        for (int k = 0; k < AD.getColumnDimension(); ++k)
        {
            WD.set(k, 0, WD.get(k, 0) *WDD.get(i, 0)/ total);
        }
        return WD;
    }

    class Sortbyscore implements Comparator<AlterNode>
    {
        public int compare(AlterNode x, AlterNode y)
        {
            if (x.score < y.score)
                return 1;
            else if (x.score > y.score)
                return -1;
            else 
                return 0;
       }
    }
    public void AltSortByScore()
    {
        Collections.sort(Alter_List,new Sortbyscore());
    }

    
    public void ComputeAdjustFactor(List<GroupDose>  DoseList, TimeSlot TimeSlot)
    {
        for (int i = 0; i < 1 + item.size()*2; ++i)
        {
            boolean zero=false;
            double max = 0.0;
            for (int j = 0; j < Alter_List.size(); ++j)
            {
                if (Alter_List.get(j).factor.get(i) == 0)
                {
                    zero = true;
                    break;
                }
            }
            if (zero)
            {
                for (int j = 0; j < Alter_List.size(); ++j)
                {
                	//Alter_List.get(j).factor.get(i)=Alter_List.get(j).factor.get(i)+1.0;
                	Alter_List.get(j).factor.set(i, new Double(Alter_List.get(j).factor.get(i)+1.0));
                	if (Alter_List.get(j).factor.get(i) > max)
                        max = Alter_List.get(j).factor.get(i);
                }
            }
            else 
            {
                for (int j = 0; j < Alter_List.size(); ++j)
                {
                    if (Alter_List.get(j).factor.get(i) > max)
                        max = Alter_List.get(j).factor.get(i);
                }               
            }
            
            for (int j = 0; j < Alter_List.size(); ++j)
            {
                //Alter_List.get(j).factor.get(i) /= max;
                Alter_List.get(j).factor.set(i,Alter_List.get(j).factor.get(i)/max);
            }
        }
    }

    public void SetAlter(List<AlterNode.AlterSubNode> tmp, List<GroupDose> DoseList)
    {
        this.SetAlter(tmp, 0, DoseList);
    }

//-------------------------------------------------------------------------------------------
//對每個DoseList中的Dose
//I.  將其每個SubDose資訊紀錄至AlterSubNode中 加入tmp List
//II. 若該Dose為DoseList中的最後一個,表示現存tmp List的內容恰為Cluster的一種組合,將tmp寫入AlterList中
//III.若不是最後一個Dose則取下一個Dose recursive call
//IV. 去除tmp中最後一個AlterSubNode,回到I.

    private void SetAlter(List<AlterNode.AlterSubNode> tmp, int DoseNum, List<GroupDose> DoseList)
    {
        for (int i = 0; i < DoseList.get(item.get(DoseNum)).SubDoseList.size(); ++i)
        {
            AlterNode.AlterSubNode NewAlterSubNode;
            NewAlterSubNode = new AlterNode.AlterSubNode(item.get(DoseNum), DoseList.get(item.get(DoseNum)).SubDoseList.get(i).Position,DoseList.get(item.get(DoseNum)).SubDoseList.get(i).TimeSlotPosition);
            tmp.add(NewAlterSubNode);


            if (DoseNum == this.item.size() - 1)
            {
                AlterNode NewAlterNode = new AlterNode();
                NewAlterNode = new AlterNode();
                NewAlterNode.GroupNumber=0;
                int temp = 0;
                Hashtable count = new Hashtable();
                for (int k = 0; k < tmp.size(); ++k)
                {
                    if (!count.contains(tmp.get(k).TimeSlotPosition))
                    {
                        ++temp;
                        count.put(tmp.get(k).TimeSlotPosition, "True");
                    }
                }

                NewAlterNode.GroupNumber = temp;
                NewAlterNode.Alter_SubList = new ArrayList<AlterNode.AlterSubNode>();
                for (int j = 0; j < tmp.size(); j++)
                {
                    AlterNode.AlterSubNode Newtmp;
                    Newtmp = new AlterNode.AlterSubNode(tmp.get(j).DoseID, tmp.get(j).SubDosePosition, tmp.get(j).TimeSlotPosition);
                    NewAlterNode.Alter_SubList.add(Newtmp);
                }
                this.Alter_List.add(NewAlterNode);
            }
            else
            {
                SetAlter(tmp, DoseNum + 1, DoseList);
            }
            
            tmp.remove(NewAlterSubNode);
        }
    }
    
    public void ClusterNode()
    {
        this.Dose_Count = 0;
        this.item = new ArrayList<Integer>();
        //int item[];
        this.Alter_List = new ArrayList<AlterNode>();
        
    }
}
