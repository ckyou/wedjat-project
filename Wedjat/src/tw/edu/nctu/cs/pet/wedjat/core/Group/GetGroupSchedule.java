package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.ArrayList;

import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;
import android.util.Log;

/*
 *  這是為了產生給專題用的schedule所用的Class
 */

public class GetGroupSchedule {
	public static  class SubDoseTime
    {
        public String subID;
        public int start;
        public int end;
        public int dose;
        public int subdose;
        public SubDoseTime() { }
        
        
        
        public SubDoseTime(String s, int st, int e)
        {
            subID = s;
            start = st;
            end = e;
        }
        @Override
        public  boolean equals(Object obj)
        {
            String s = (String)obj;
            return subID.equals(obj);
        }
        @Override
        public int hashCode()
        {
            return subID.hashCode();
        }

    }
    public class FinalSchedule
    {
        public MedicationDirection m;
        public int startTime;
        public int endTime;
    }
    public static class GroupSchedule
    {
        public ArrayList<SubDoseTime> subSlotTimes = new ArrayList<SubDoseTime>();
        public ArrayList<ArrayList<String>> allSchedule = new ArrayList<ArrayList<String>>();
        
        public SubDoseTime findSubDoseTime(ArrayList<SubDoseTime> s,String n){
        	Log.d("TEST","fSDT");
        	int i = 0;
        	while(i < s.size()){
        		Log.d("IsReturnNull","NO."+i);
        		Log.d("IsReturnNull",s.get(i).subID + "==" + n + "     " + s.get(i).subID.equals(n));
        		if(s.get(i).subID.equals(n)){                              //(s.get(i).subID==n)
        			Log.d("IsReturnNull","N"+i);
        			return s.get(i);
        		}
        		i++;
        	}
        	Log.d("IsReturnNull","Y"+i);
        	return null;
        }
      
//        public void printsubSlotTimes()
//        {   
//            for (SubDoseTime s : subSlotTimes)
//            {
//                Console.WriteLine(s.subID + " " + s.start + " " + s.end);
//            }
//        }
       
        public SubDoseTime getSubDoseTime(String subID)
        {
            //return (subSlotTimes.Find(delegate(SubDoseTime p) { return (p.subID.Equals(subID)); }));
        	Log.d("TEST","gSDT");
        	return findSubDoseTime(subSlotTimes,subID);
        }

    }
    
    
}
