package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.ArrayList;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;

public class GroupDose {

    public int medID, dose_size, SubDose_Count, StartTime, EndTime;
    public MedicationDirection medicine = new MedicationDirection();

    //-----------------------------------------------------------------
    //此處的medID和其他地方不同，記錄的是Dose原始的編號
    //其他TimeSlot.TimeLine.Cluster中的medID都是記錄Dose在Dose list中的位置

    public List<SubDoseNode> SubDoseList;

    //ICloneable
    public GroupDose() { }
    public GroupDose(int medID, int starttime, int endtime, int dose_size, MedicationDirection m)
    {
        this.medID = medID;
        this.StartTime = starttime;
        this.EndTime = endtime;
        this.dose_size = dose_size;
        this.SubDose_Count = 0;
        this.medicine = m;
        SubDoseList = new ArrayList<SubDoseNode>();
    }
}
