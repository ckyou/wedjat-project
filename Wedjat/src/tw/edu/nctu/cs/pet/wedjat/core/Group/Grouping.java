package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.util.ArrayList;

import android.util.Log;

import tw.edu.nctu.cs.pet.wedjat.core.Group.GetGroupSchedule.GroupSchedule;
import tw.edu.nctu.cs.pet.wedjat.core.Group.GetGroupSchedule.SubDoseTime;
import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;
import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicineInput;
import tw.edu.nctu.cs.pet.wedjat.core.startpoint.Engine;
import tw.edu.nctu.cs.pet.wedjat.gpb.*;
import tw.edu.nctu.cs.pet.wedjat.utility.Config;

public class Grouping  {
	//Group 之部份
	
    public Cluster group_start(ArrayList<GroupDose> DoseList)
    {
        /*StreamWriter write;
        write = File.CreateText("..\\..\\Result\\GroupingResult.txt");
        write.Close();
        write = File.AppendText("..\\..\\Result\\GroupingResult.txt");*/
    	try{
    	Log.d("TEST","group_start");
        //File path=getExternalFilesDir(null);
        //String pathForRead = path.toString();
       	//String pathForReadFile = pathForRead + "/GroupingResult.txt";
        //File GroupingResult = new File(pathForReadFile);
    	
       
        //File GroupingResult = new File(getExternalFilesDir(null), "GroupingResult.txt");
        
        
       
    	String pathForRead = Engine.path.toString();
    	pathForRead += "/GroupingResult.txt";
    	File GroupingResult = new File(pathForRead);
        
        
        /*StreamWriter wirteSchedule;
        wirteSchedule = File.CreateText("..\\..\\Result\\GroupedSchedule.txt");
        wirteSchedule.Close();
        wirteSchedule = File.AppendText("..\\..\\Result\\GroupedSchedule.txt");*/
        
        //pathForReadFile = pathForRead + "/GroupedSchedule.txt";
        //File GroupingSchedule = new File(pathForReadFile);
        File GroupingSchedule = new File(Engine.path, "GroupingSchedule.txt");
    	//String pathForRead2 = Engine.path.toString();
    	//pathForRead2 += "/GroupedSchedule.txt";
    	//File GroupingSchedule = new File(pathForRead2);
        
        TimeLine TimeLine = new TimeLine();
        Cluster Clusterlist = new Cluster();
        TimeSlot TimeSlot = new TimeSlot();
        GroupSchedule schedule = new GroupSchedule();
        
        FileWriter GRwriter;
        GRwriter = new FileWriter(GroupingResult);
        
        FileWriter GSwriter;
        GSwriter = new FileWriter(GroupingSchedule);
        Log.d("TEST",GroupingSchedule.getAbsolutePath());
        
        GSwriter.write(Engine.DURATION + "\r\n");
        GSwriter.write(0 + "\r\n");//暫定起始點都是0
       // fwriter.close();
        /*
         * By 方鍾豪 
         */


        /*
        //-----------------計算比例之簡單測資-------------
      
        GroupDose Adose;
        Adose = new GroupDose(1, 0, 6, 0, A);
        GroupDose Bdose;
        Bdose = new GroupDose(2, 4, 8, 0, B);

        DoseList.Add(Adose);
        DoseList.Add(Bdose);
        

        //-----------------複雜測資-------------
          /*
        Schedule Adose;
        Adose = new Schedule(1, 0, 6, 0,B);
        Schedule Bdose;
        Bdose = new Schedule(2, 0, 4, 0,C);
        Schedule Cdose;
        Cdose = new Schedule(3, 8, 16, 0,A);
        Schedule Ddose;
        Ddose = new Schedule(4, 8, 12, 0,C);
        Schedule Edose;
        Edose = new Schedule(5, 12, 18, 0,B);
        Schedule Fdose;
        Fdose = new Schedule(6, 16, 20, 0,C);
                    
        DoseList.Add(Adose);
        DoseList.Add(Bdose);
        DoseList.Add(Cdose);
        DoseList.Add(Ddose);
        DoseList.Add(Edose);
        DoseList.Add(Fdose);
        /*
        //------------------簡單測資------------------
        /*Schedule Adose;
        Adose = new Schedule(1, 0, 4, 0);
        Schedule Bdose;
        Bdose = new Schedule(2, 4, 7, 0);
        Schedule Cdose;
        Cdose = new Schedule(3, 6, 8, 0);

        DoseList.Add(Adose);
        DoseList.Add(Bdose);
        DoseList.Add(Cdose);*/



        //fwriter = new FileWriter(GroupingResult); 
        GRwriter.write("-------Dose Data-------" + "\r\n");

        
        
        for (int i = 0; i < DoseList.size(); i++)
        {
        	//Drugs.Dose.Builder ddtemp = Drugs.Dose.newBuilder(); //gpb++
        	
            String tmp;
            tmp = "DoseID = " + DoseList.get(i).medID + "\r\n";
            
            //ddtemp.setID(DoseList.get(i).medID);                 //gpb++
            
            //MedicineInput.DrugList.get(DoseList.get(i).medicine.Med_ID).addDose(ddtemp); //gpb++
            
            GRwriter.write(tmp);                
            tmp = "Start = " + DoseList.get(i).StartTime + "\r\n";
            GRwriter.write(tmp);
            tmp = "End = " + DoseList.get(i).EndTime + "\r\n\r\n";
            GRwriter.write(tmp);
        }
        

        //------------------------------------------------------------------
        //                    以下建構TimeLine，並Output

        GRwriter.write("-------TimeLine Data-------" + "\r\n");
        TimeLine.ConstructTimeLine(DoseList);
        for (int i = 0; i < TimeLine.time_line.size(); i++)
        {
            String tmp;
            tmp = "Time = " + TimeLine.time_line.get(i).time + "\r\n";
            GRwriter.write(tmp);
            tmp = "Start : ";
            GRwriter.write(tmp);
            for (int j = 0; j < TimeLine.time_line.get(i).start.size(); j++)
            {
                tmp = DoseList.get(TimeLine.time_line.get(i).start.get(j)).medID + "->";
                GRwriter.write(tmp);
            }

            tmp = "\r\n" + "End : ";
            GRwriter.write(tmp);
            for (int j = 0; j < TimeLine.time_line.get(i).end.size(); j++)
            {
                tmp = DoseList.get(TimeLine.time_line.get(i).end.get(j)).medID + "->";
                GRwriter.write(tmp);
            }
            tmp = "\r\n\r\n";
            GRwriter.write(tmp);
        }

        //-------------------------------------------------------------------
        //           以下建構Cluster，並Output
        Clusterlist.ConstructCluster(TimeLine); //file被清空???
        //GRwriter.write("testest");

        GRwriter.write("-------Cluster Data-------" + "\r\n");
        for (int i = 0; i < Clusterlist.ClusterList.size(); i++)
        {
            String tmp;
            tmp = "Dose Count = " + Clusterlist.ClusterList.get(i).Dose_Count + "\r\n";
            GRwriter.write(tmp);
            tmp = "In Cluster : ";
            GRwriter.write(tmp);
            for (int j = 0; j < Clusterlist.ClusterList.get(i).item.size(); j++)
            {
                tmp = DoseList.get(Clusterlist.ClusterList.get(i).item.get(j)).medID + "->";
                GRwriter.write(tmp);
            }
            tmp = "\r\n\r\n";
            GRwriter.write(tmp);
        }

        //----------------------------------------------------------------
        //      以下建構TimeSlot，並Output
        TimeSlot.ConstructTimeSlotList(TimeLine, DoseList);
        
        for(int i=0;i<TimeSlot.TimeSlotList.size();i++) //設定protobuf的TimeSlot
        {
        	MedicineInput.prescription.addTimeSlotBuilder();
        	MedicineInput.prescription.getTimeSlotBuilder(i).setID(i);
        	MedicineInput.prescription.getTimeSlotBuilder(i).setStartTime(TimeSlot.TimeSlotList.get(i).Start_Time);
        	MedicineInput.prescription.getTimeSlotBuilder(i).setEndTime(TimeSlot.TimeSlotList.get(i).End_Time);
        	
        	/*TimeSlotList.add(Drugs.TimeSlot.newBuilder());
        	TimeSlotList.get(i).setID(i);
        	TimeSlotList.get(i).setStartTime(TimeSlot.TimeSlotList.get(i).Start_Time);
        	TimeSlotList.get(i).setEndTime(TimeSlot.TimeSlotList.get(i).End_Time);*/
        }
        
        /*Log.d("gpb", "==TimeSlotList==");
        
        for(int i=0;i<TimeSlotList.size();i++)
        {
        	Log.d("gpb", TimeSlotList.get(i).getID() + ", " + TimeSlotList.get(i).getStartTime() + ", " + TimeSlotList.get(i).getEndTime());
        }*/

        GRwriter.write("-------TimeSlot Data-------" + "\r\n");
        for (int i = 0; i < TimeSlot.TimeSlotList.size(); i++)
        {
            String tmp;

            tmp = "Time Slot " + i + " : \r\n";
            GRwriter.write(tmp);
            tmp = "Start time = " + TimeSlot.TimeSlotList.get(i).Start_Time + "\r\n";
            GRwriter.write(tmp);
            tmp = "End time = " + TimeSlot.TimeSlotList.get(i).End_Time + "\r\n";
            GRwriter.write(tmp);
            tmp = "In Time Slot : ";
            GRwriter.write(tmp);
            for (int j = 0; j < TimeSlot.TimeSlotList.get(i).item.size(); j++)
            {
                tmp = "" + DoseList.get(TimeSlot.TimeSlotList.get(i).item.get(j).DoseID).medID;
                tmp += TimeSlot.TimeSlotList.get(i).item.get(j).SubDose.Position + "->";
                GRwriter.write(tmp);

                //for getGroupSchedule
                SubDoseTime sdt = new SubDoseTime();
                sdt.start = TimeSlot.TimeSlotList.get(i).Start_Time;
                sdt.end = TimeSlot.TimeSlotList.get(i).End_Time;
                sdt.subID = "" + DoseList.get(TimeSlot.TimeSlotList.get(i).item.get(j).DoseID).medID;
                sdt.subID += TimeSlot.TimeSlotList.get(i).item.get(j).SubDose.Position;
                sdt.dose = DoseList.get(TimeSlot.TimeSlotList.get(i).item.get(j).DoseID).medID;
                sdt.subdose = TimeSlot.TimeSlotList.get(i).item.get(j).SubDose.Position;
                schedule.subSlotTimes.add(sdt);
            }
            tmp = "\r\n\r\n";
            GRwriter.write(tmp);
        }

        //----------------------------------------------------------------
        //        以下建構AlternativeList，並且計算weight和各項數值以及最後的score， 最後Output所有可能的服藥list，並且按照分數大小
        //  by 方鍾豪

        Clusterlist.SetAlter(DoseList);

        for (ClusterNode cn : Clusterlist.ClusterList) //建立MESSAGE相關參數 start(開始時間)  end(結束時間) medicine
        {
            for (ClusterNode.AlterNode an : cn.Alter_List)
            { 
                for(ClusterNode.AlterNode.AlterSubNode asn : an.Alter_SubList )
                {
                    asn.start = TimeSlot.TimeSlotList.get(asn.TimeSlotPosition).Start_Time;
                    asn.end = TimeSlot.TimeSlotList.get(asn.TimeSlotPosition).End_Time;
                    asn.medicine = DoseList.get(asn.DoseID).medicine;
                }
            }
        }

        for (int i = 0; i < Clusterlist.ClusterList.size(); ++i)
        {
            Clusterlist.ClusterList.get(i).ComputeWeight(DoseList);
        }
        for (int i = 0; i < Clusterlist.ClusterList.size(); ++i)
        {
            for (int j = 0; j < Clusterlist.ClusterList.get(i).Alter_List.size(); ++j)
            {
                Clusterlist.ClusterList.get(i).Alter_List.get(j).ComputeFactor(DoseList, TimeSlot);
            }
            Clusterlist.ClusterList.get(i).ComputeAdjustFactor(DoseList, TimeSlot);
            Clusterlist.ClusterList.get(i).ComputeScore();
            Clusterlist.ClusterList.get(i).AltSortByScore();
        }

        
        /*Schedule.Builder scheduleData = Schedule.newBuilder();
        scheduleData.setRange(Engine.DURATION);
        
        for(int i=0;i<Clusterlist.ClusterList.size();i++){
        	Drug.Builder temp = Drug.newBuilder();
        	temp.setName(Clusterlist.ClusterList.get(i).Alter_List.get(0).Alter_SubList.get(0).medicine.Med_Name);
        	temp.setStartTime(Clusterlist.ClusterList.get(i).Alter_List.get(0).Alter_SubList.get(0).start);
        	temp.setEndTime(Clusterlist.ClusterList.get(i).Alter_List.get(0).Alter_SubList.get(0).end);
        	Alternative.Builder alterData = Alternative.newBuilder();
        	alterData.addDrug(temp);
        	scheduleData.addAlternative(alterData);
		}*/
        
        /*for(int i=0;i<Clusterlist.ClusterList.size();i++){
			Log.d("TEST", "print");
			Log.d("TEST", scheduleData.getAlternative(i).getDrug(0).getName() + ", " + scheduleData.getAlternative(i).getDrug(0).getStartTime() + ", " + scheduleData.getAlternative(i).getDrug(0).getEndTime());
		}*/

        GRwriter.write("-------Alternative Data-------" + "\r\n");
        for (int i = 0; i < Clusterlist.ClusterList.size(); i++)
        {
            String tmp;
             
            schedule.allSchedule=new  ArrayList<ArrayList<String>>();
            tmp = "Cluster " + i + " : \r\n";
            GRwriter.write(tmp);
            tmp = "Weight\r\n  群組個數  : " + Clusterlist.ClusterList.get(i).weight.get(0) + "\r\n";
            GRwriter.write(tmp);
            for (int j = 1; j < Clusterlist.ClusterList.get(i).weight.size(); ++j)
            {
                tmp = "";
                tmp += ((j % 2) == 0) ? "  剩餘次數 D" : "  時間段長 D";
                tmp+=(Clusterlist.ClusterList.get(i).item.get((j-1) / 2)+1)+"  : ";
                tmp += Clusterlist.ClusterList.get(i).weight.get(j).toString() + "\r\n";
                GRwriter.write(tmp);
            }

           
            for (int j = 0; j < Clusterlist.ClusterList.get(i).Alter_List.size(); j++)
            {
                tmp = "Alternative " + j + " : ";
                GRwriter.write(tmp);
                ArrayList<String> ltmp = new ArrayList<String>();
                for (int k = 0; k < Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.size(); k++)
                {
                    tmp = DoseList.get(Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).DoseID).medID+" ";
                    tmp += Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).SubDosePosition  + "-> \t";
                    ltmp.add("" + DoseList.get(Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).DoseID).medID + Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).SubDosePosition);
                    GRwriter.write(tmp);
                }
                schedule.allSchedule.add(ltmp);
                tmp = "\r\nGroups : " + Clusterlist.ClusterList.get(i).Alter_List.get(j).factor.get(0);
                //Log.d("QOO", "5566:" + Clusterlist.ClusterList.get(i).Alter_List.get(j).factor.get(0));
                for (int k = 1; k < Clusterlist.ClusterList.get(i).Alter_List.get(j).factor.size(); ++k)
                {
                    tmp += (((k) % 2) == 0) ? "  剩餘次數 D" : "  時間段長 D";
                    tmp += Clusterlist.ClusterList.get(i).item.get((k - 1) / 2) + " : ";
                    tmp += Clusterlist.ClusterList.get(i).Alter_List.get(j).factor.get(k).toString();
                }
                tmp +="  Score : "+ Clusterlist.ClusterList.get(i).Alter_List.get(j).score;
                tmp += "\r\n\r\n";
                GRwriter.write(tmp);
            }

            tmp = "\r\n";
            GRwriter.write(tmp);
            //fwriter.close();
            //fwriter = new FileWriter(GroupingResult);
        
            //schedule.printsubSlotTimes(); //方法已被註解??
            SubDoseTime s = schedule.getSubDoseTime("11"); /////????????????????
            System.out.println(s.start + " " + s.end);


             //wirteSchedule.WriteLine("-------Schedule-------");
            GSwriter.write("@" + "\r\n");

             boolean isFirst = true;
                 for(ArrayList<String> l : schedule.allSchedule)
                 {
                      //Console.WriteLine("------------------------");
                      //wirteSchedule.Write("# ");
                      ArrayList<TimeInterval>  tmpGroup = new ArrayList<TimeInterval>();
                      boolean more = true;
                      
                      for (String ss : l) //第一個schedule的時程
                      {
                         SubDoseTime tmpSlot = schedule.getSubDoseTime(ss);
                         System.out.println(ss);
                         Log.d("getGS", ss);
                         if (tmpGroup.size() == 0)
                         {
                             TimeInterval ti = new TimeInterval(tmpSlot.start, tmpSlot.end);
                            // if (tmpSlot.dose>0)
                            //    ti.medicines.Add(DoseList[tmpSlot.dose-1].medicine);//??????????????????? -1
                            // else
                                 ti.medicines.add(DoseList.get(tmpSlot.dose).medicine);//??????????????????? -1
                             
                             tmpGroup.add(ti);
                         }
                         else
                         {
                             for (TimeInterval ti : tmpGroup)
                             {
                                 if (ti.start == tmpSlot.start && ti.end == tmpSlot.end)
                                 {
                                     ti.medicines.add(DoseList.get(tmpSlot.dose).medicine);
                                     more = false;
                                     break;
                                 }
                             }
                             if(more)
                             {
                                TimeInterval tti = new TimeInterval(tmpSlot.start, tmpSlot.end);
                                tti.medicines.add(DoseList.get(tmpSlot.dose).medicine);
                                 tmpGroup.add(tti);
                             }
                         }
                     }
                     GSwriter.write("# ");
                     for (TimeInterval ti : tmpGroup)
                     {
                         if (isFirst)
                         {
                             //wirteSchedule.Write ("First ");
                            //wirteSchedule.Write("# ");
                             isFirst = false;
                         }
                         GSwriter.write(ti.start + " " + ti.end+" ");
                         for (MedicationDirection m : ti.medicines)
                         {
                        	 GSwriter.write(m.Med_Name + " ");
                         }
                         GSwriter.write("\r\n");
                     }
                     //wirteSchedule.WriteLine("------------------------");
                     
                     
                  
             }
        }//cluster
         //Console.WriteLine("asdasd"+Clusterlist.ClusterList[0].Alter_List[1].factor[0].ToString());
        //fwriter.write("sfsfdfddfd\n");
        //fwriter.flush();
        
        //----End of set gpb----
        
        ArrayList<Prescriptions.Dose.Builder> pbDoseList = new ArrayList<Prescriptions.Dose.Builder>();
        
        for(int i=0;i<DoseList.size();i++)
        {
        	pbDoseList.add(Prescriptions.Dose.newBuilder());
        	pbDoseList.get(i).setId(DoseList.get(i).medID);
        }
        
        for (int i = 0; i < Clusterlist.ClusterList.size(); i++)
        {
        	MedicineInput.prescription.addClusterBuilder();  //pbClusterList.add(Drugs.Cluster.newBuilder());
        	
        	ArrayList<Integer> match = new ArrayList<Integer>();
        	
        	for (int j = 0; j < Clusterlist.ClusterList.get(i).Alter_List.size(); j++)
        	{
        		Prescriptions.Group.Builder temp = Prescriptions.Group.newBuilder(); //Drugs.Group.Builder temp = Drugs.Group.newBuilder();
        		temp.setId(j);
        		temp.setScore(Clusterlist.ClusterList.get(i).Alter_List.get(j).score);
        		
        		if(j==0){
        			for (int k = 0; k < Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.size(); k++)
            		{
        				match.add(Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).DoseID);
            		}
        		}

        		for (int k = 0; k < Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.size(); k++)
        		{
        			temp.addDoseId(Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).DoseID);
        			temp.addTimeSlotId(Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).TimeSlotPosition);
        			
        			
        			/*temp.setDose(k, Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).DoseID);
        			temp.setTimeSlot(k, Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).TimeSlotPosition);
        			temp.setDrugID(k, Clusterlist.ClusterList.get(i).Alter_List.get(j).Alter_SubList.get(k).medicine.Med_ID);*/
        		}
        		
        		MedicineInput.prescription.getClusterBuilder(i).addGroup(temp);  //pbClusterList.get(i).addGroup(temp);
        		
        		//Log.d("gpb", "match: " + match.size());
        		
        		for(int k=0;k<match.size();k++)
        		{
        			Prescriptions.InTakeId.Builder ititemp = Prescriptions.InTakeId.newBuilder();
        			ititemp.setClusterId(i);
        			ititemp.setGroupId(temp.getId());
        			pbDoseList.get(match.get(k)).addInTakeId(ititemp); //pbDoseList.get(match.get(k)).addGroup(temp);
        		}
        		
        	}
        	match.clear();
        }
        
        for (int i = 0; i < DoseList.size(); i++)
        {
        	MedicineInput.prescription.getDrugBuilder(DoseList.get(i).medicine.Med_ID).addDose(pbDoseList.get(i));
        	
        	//MedicineInput.DrugList.get(DoseList.get(i).medicine.Med_ID).addDose(pbDoseList.get(i));
        }
        
        //=====以下為protobuf test=====
        
        
        for(int i=0;i<MedicineInput.prescription.getDrugCount();i++)
        {
        	Log.d("gpb", "Drug ID= " + MedicineInput.prescription.getDrugBuilder(i).getId());
        	
        	//Log.d("gpb", "Drug ID= " + MedicineInput.DrugList.get(i).getID());
        	
        	for(int j=0;j<MedicineInput.prescription.getDrugBuilder(i).getDoseCount();j++)
        	//for(int j=0;j<MedicineInput.DrugList.get(i).getDoseCount();j++)
        	{
        		Log.d("gpb", "   " + "Dose ID= " + MedicineInput.prescription.getDrug(i).getDoseOrBuilder(j).getId());
        		//Log.d("gpb", "   " + "Dose ID= " + MedicineInput.DrugList.get(i).getDoseBuilder(j).getID());
        		
        		for(int k=0;k<MedicineInput.prescription.getDrugBuilder(i).getDoseBuilder(j).getInTakeIdCount();k++)
        		//for(int k=0;k<MedicineInput.DrugList.get(i).getDoseBuilder(j).getGroupCount();k++)
        		{
        			Prescriptions.InTakeId.Builder temp = MedicineInput.prescription.getDrugBuilder(i).getDoseBuilder(j).getInTakeIdBuilder(k);
        			
        			Log.d("gpb", "      " + "Group ID= " + MedicineInput.prescription.getClusterBuilder(temp.getClusterId()).getGroupBuilder(temp.getGroupId()).getId());
        			//Log.d("gpb", "      " + "Group ID= " + MedicineInput.DrugList.get(i).getDoseBuilder(j).getGroupBuilder(k).getID());
        			Log.d("gpb", "      " + "Group ID= " + MedicineInput.prescription.getClusterBuilder(temp.getClusterId()).getGroupBuilder(temp.getGroupId()).getScore());
        			
        			for(int l=0;l<MedicineInput.prescription.getClusterBuilder(temp.getClusterId()).getGroupBuilder(temp.getGroupId()).getDoseIdCount();l++)
        			{
        				Log.d("gpb", "         " + "Dose= " + MedicineInput.prescription.getClusterBuilder(temp.getClusterId()).getGroupBuilder(temp.getGroupId()).getDoseId(l));
        				Log.d("gpb", "         " + "TimeSlot= " + MedicineInput.prescription.getClusterBuilder(temp.getClusterId()).getGroupBuilder(temp.getGroupId()).getTimeSlotId(l));
        			}
        		}
        	}
        }
        
        //Writing prescription to a file
        File gpb_output = new File(Engine.path, Config.scheduleName);
        FileOutputStream output = new FileOutputStream(gpb_output);
        MedicineInput.prescription.build().writeTo(output);
        output.close();
        
        //Test the file
        /*
        Log.d("gpb", "==========Test Output from file==========");
        
        Prescription test = Prescription.parseFrom(new FileInputStream(Engine.path + "/Protobuf_Output.txt"));
        Log.d("gpb", "==========Test Output from file==========");
        for(int i=0;i<test.getDrugCount();i++)
        {
        	Log.d("gpb", "Drug ID= " + test.getDrug(i).getId());
        	
        	//Log.d("gpb", "Drug ID= " + MedicineInput.DrugList.get(i).getID());
        	
        	for(int j=0;j<test.getDrug(i).getDoseCount();j++)
        	//for(int j=0;j<MedicineInput.DrugList.get(i).getDoseCount();j++)
        	{
        		Log.d("gpb", "   " + "Dose ID= " + test.getDrug(i).getDoseOrBuilder(j).getId());
        		//Log.d("gpb", "   " + "Dose ID= " + MedicineInput.DrugList.get(i).getDoseBuilder(j).getID());
        		
        		for(int k=0;k<test.getDrug(i).getDose(j).getInTakeIdCount();k++)
        		//for(int k=0;k<MedicineInput.DrugList.get(i).getDoseBuilder(j).getGroupCount();k++)
        		{
        			Prescriptions.InTakeId temp = test.getDrug(i).getDose(j).getInTakeId(k);
        			
        			Log.d("gpb", "      " + "Group ID= " + test.getCluster(temp.getClusterId()).getGroup(temp.getGroupId()).getId());
        			//Log.d("gpb", "      " + "Group ID= " + MedicineInput.DrugList.get(i).getDoseBuilder(j).getGroupBuilder(k).getID());
        			Log.d("gpb", "      " + "Group ID= " + test.getCluster(temp.getClusterId()).getGroup(temp.getGroupId()).getScore());
        			
        			for(int l=0;l<test.getCluster(temp.getClusterId()).getGroup(temp.getGroupId()).getDoseIdCount();l++)
        			{
        				Log.d("gpb", "         " + "Dose= " + test.getCluster(temp.getClusterId()).getGroup(temp.getGroupId()).getDoseId(l));
        				Log.d("gpb", "         " + "TimeSlot= " + test.getCluster(temp.getClusterId()).getGroup(temp.getGroupId()).getTimeSlotId(l));
        			}
        		}
        	}
        }
        */
        
      
        GRwriter.close();
        GSwriter.close();
        return Clusterlist;
        }
        catch(Exception e){
        	e.printStackTrace();
        }
    	
    	Log.d("TEST","Groupping OK");
    	
		return null;
		
		
    }
}



class TimeInterval
{
    public int start;
    public int end;
    public ArrayList<MedicationDirection> medicines = new ArrayList<MedicationDirection>();
    public TimeInterval(int s, int e)
    {
        start = s;
        end = e;
    }
}
