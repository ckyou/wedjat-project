package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.Comparator;

//---------------------------------------------------------
//利用IComparer將Timeline以時間排序

public  class Icptime implements Comparator<TimeNode>
{
  public int compare(TimeNode x, TimeNode y)
  {
      return new Integer(x.time).compareTo(y.time);
  }


}


//---------------------------------------------------------------
//將DoseList(預設是按開始時間排序)的每個Dose時間資料紀錄
//若為新的時間點就加入一個新的Time Node，如果是已經有的就直接把記錄到Start或End中
//最後再把整個Time Line依時間排序