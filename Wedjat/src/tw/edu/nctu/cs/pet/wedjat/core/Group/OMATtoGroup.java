package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.ArrayList;

import tw.edu.nctu.cs.pet.wedjat.core.OMAT.Dose;
import tw.edu.nctu.cs.pet.wedjat.core.OMAT.Job;
import tw.edu.nctu.cs.pet.wedjat.core.OMAT.Schedule;

public class OMATtoGroup {
	public ArrayList<GroupDose> genGroupDoseList(Schedule schedule)
    {
        ArrayList<GroupDose> DoseList=new ArrayList<GroupDose>();
        int num = 0;
        for (Job j : schedule.jobs)
        {
            for (Dose d : j.doses)
            {
                DoseList.add(new GroupDose(num, d.start_time, d.end_time,1, j.medicine));//
                ++num;
            }
                
        }
        return DoseList;
    }
}
