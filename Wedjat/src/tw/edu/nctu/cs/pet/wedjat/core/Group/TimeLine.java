package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.util.Log;

public class TimeLine {	
	
	public ArrayList<TimeNode> time_line = new ArrayList<TimeNode>();
	
	public TimeNode findTimeNode(ArrayList<TimeNode> s,int n){
    	int i = 0;
    	while(i < s.size()){
    		if(s.get(i).time==n) return s.get(i);
    		i++;
    	}
    	return null;
    }

    public void ConstructTimeLine(List<GroupDose> DoseList) 
    {            
        for (int i = 0; i < DoseList.size(); i++)
        {
            int starttmp = DoseList.get(i).StartTime;
            int endtmp = DoseList.get(i).EndTime;

            //TimeNode tn = time_line.Find(delegate(TimeNode s) { return (s.time == starttmp); });
            TimeNode tn = findTimeNode(time_line,starttmp);
            Log.d("findTimeNode", i + " " + String.valueOf(tn != null));
            
            
            if (tn != null)
            {
                TimeNode tmp;
                //tmp = time_line.Find(delegate(TimeNode s) { return (s.time == starttmp); });
                tmp = findTimeNode(time_line,starttmp);
                
                TimeSubNode newsubnode = new TimeSubNode();
                //newsubnode.medID = i;
                //tmp.start.add(newsubnode);
                tmp.start.add(i);
            }
            else
            {
                TimeNode newnode = new TimeNode(starttmp);
                TimeSubNode newsubnode = new TimeSubNode();
                //newsubnode.medID = i;
                //newnode.start.add(newsubnode);
                newnode.start.add(i);
                time_line.add(newnode);
            }

            //tn = time_line.Find(delegate(TimeNode e) { return (e.time == endtmp); });
            tn = findTimeNode(time_line,endtmp);

            if (tn != null)
            {
                TimeNode tmp;
                //tmp = time_line.Find(delegate(TimeNode e) { return (e.time == endtmp); });
                tmp = findTimeNode(time_line,endtmp);
                TimeSubNode newsubnode = new TimeSubNode();
                //newsubnode.medID = i;
                //tmp.end.add(newsubnode);
                tmp.end.add(i);
            }
            else
            {
                TimeNode newnode = new TimeNode(endtmp);
                TimeSubNode newsubnode = new TimeSubNode();
                //newsubnode.medID = i;
                //newnode.end.add(newsubnode);
                newnode.end.add(i);
                time_line.add(newnode);
            }

            Collections.sort(time_line,new Icptime());
            
        }
        /*for(int i=0;i<time_line.size();i++){
        	Log.d("QOO", "==" + i + "==");
        	Log.d("QOO", "start:");
        	for(int j=0;j<time_line.get(i).start.size();j++){
        		Log.d("QOO", "" + time_line.get(i).start.get(j));
        	}
        	Log.d("QOO", "end:");
        	for(int j=0;j<time_line.get(i).end.size();j++){
        		Log.d("QOO", "" + time_line.get(i).end.get(j));
        	}
        	Log.d("QOO", "=====");
        }*/
    }
}




