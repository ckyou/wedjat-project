package tw.edu.nctu.cs.pet.wedjat.core.Group;

import java.util.ArrayList;
import java.util.List;

import android.util.Log;

public class TimeSlot {
	public List<TimeSlotNode> TimeSlotList = new ArrayList<TimeSlotNode>();


    //-----------------------------------------------------------------
    //掃描整個Timeline中的每個時間點
    //1. 當tmplist不是空的的時候，就加入一個新的TimeSlot
    //2. 把每個在tmplist中的dose都新加一個subdose，並把這個新的subdose加到目前的TimeSlot
    //3.把那個時間點狀態為start的dose都加到tmplist中，end的dose就從tmplist中刪除

    public void ConstructTimeSlotList(TimeLine timeline, List<GroupDose> DoseList)
    {
        ArrayList<Integer> tmplist;
        tmplist = new ArrayList<Integer>();
        int timetmp = 0;

        for (int i = 0; i < timeline.time_line.size(); i++)
        {
            if (tmplist.size() != 0)
            {
                TimeSlotNode newnode;
                newnode = new TimeSlotNode(timetmp, timeline.time_line.get(i).time);
                TimeSlotList.add(newnode);
            }

            for (int j = 0; j < tmplist.size(); j++)
            {
                DoseList.get(tmplist.get(j)).SubDose_Count++;
                SubDoseNode subdosenewnode = new SubDoseNode();
                subdosenewnode.Position = DoseList.get(tmplist.get(j)).SubDose_Count;   //Subdose Position 從1開始
                subdosenewnode.TimeSlotPosition = TimeSlotList.size()-1;
                DoseList.get(tmplist.get(j)).SubDoseList.add(subdosenewnode);

                TimeSlotSubNode timeslotnewnode = new TimeSlotSubNode();
                timeslotnewnode.SubDose = subdosenewnode;
                timeslotnewnode.DoseID = tmplist.get(j);
                TimeSlotList.get(TimeSlotList.size() - 1).item.add(timeslotnewnode);
            }

            for (int j = 0; j < timeline.time_line.get(i).start.size(); j++)
            {
                tmplist.add(timeline.time_line.get(i).start.get(j));
            }
            
            /*Log.d("clear??", "before:" + timeline.time_line.get(i).end.size());
            
            timeline.time_line.get(i).end.clear();
            
            Log.d("clear??", "after:" + timeline.time_line.get(i).end.size());*/

            for (int j = 0; j < timeline.time_line.get(i).end.size(); j++)
            {
                //tmplist.remove(timeline.time_line.get(i).end.get(j)); /////////
                //tmplist.remove(timeline.time_line.get(i).end.get(timeline.time_line.get(i).end.indexOf(timeline.time_line.get(i).end.get(j))));
            	int remove_data = timeline.time_line.get(i).end.get(j);
            	Log.d("clear??", "5566");
            	tmplist.remove(tmplist.indexOf(remove_data));
            	Log.d("clear??", "7788");
            }
            
            

            timetmp = timeline.time_line.get(i).time;
        }
    }
}
