package tw.edu.nctu.cs.pet.wedjat.core.Input;

public class DosageParameters {
	
	public int nsmin, nsmax, asmin, asmax, budget, replenishment, lowerbound, pinterval, tmin, tmax;
    //IClonable
    public DosageParameters() { }
    private DosageParameters(int nsmax, int nsmin, int asmax, int asmin, int budget, int replenishment, int lowerbound, int pinterval, int tmin, int tmax)
    {
        this.nsmax = nsmax; this.nsmin = nsmin; this.asmax = asmax; this.asmin = asmin; this.tmin = tmin; this.tmax = tmax;
        this.budget = budget; this.replenishment = replenishment; this.lowerbound = lowerbound; this.pinterval = pinterval;
    }
    public Object Clone()
    {
        return new DosageParameters(this.nsmax, this.nsmin, this.asmax, this.asmin, this.budget, this.replenishment, this.lowerbound, this.pinterval, this.tmin, this.tmax);
    }
    
}
