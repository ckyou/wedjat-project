package tw.edu.nctu.cs.pet.wedjat.core.Input;

public interface Importance {
	public static final int IMPORTANT=1;
	public static final int ORDINARY=2;
	public static final int NONNECESSARY=3;
}
