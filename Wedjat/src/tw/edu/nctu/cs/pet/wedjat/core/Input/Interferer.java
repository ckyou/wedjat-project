package tw.edu.nctu.cs.pet.wedjat.core.Input;

public class Interferer {
	public int MedID, minToInterferer, minFrInterferer;
    //ICloneable
    public Interferer() { }
    private Interferer(int MedID, int minToInterferer, int minFrInterferer)
    {
        this.MedID = MedID; this.minToInterferer = minToInterferer; this.minFrInterferer = minFrInterferer;
    }
    public Object Clone()
    {
        return new Interferer(this.MedID, this.minToInterferer, this.minFrInterferer);
    }
}
