package tw.edu.nctu.cs.pet.wedjat.core.Input;

import tw.edu.nctu.cs.pet.wedjat.core.OMAT.ResourceModel;
import tw.edu.nctu.cs.pet.wedjat.core.startpoint.Engine;

public class MedicationDirection {
	public int Med_ID;
    public String Med_Name;
    public String Granularity;
    public DosageParameters DP;
    public SpecialInstructions SI;
    public ResourceModel RM;
    public int type;
    public int property;
    //ICloneable
    public MedicationDirection() { }



    private MedicationDirection(int Med_ID, String Med_Name, String Granularity, DosageParameters DP, SpecialInstructions SI, ResourceModel RM,int im,int p)
    {
        this.Med_ID = Med_ID;
        this.Med_Name = Med_Name;
        this.Granularity = Granularity;
        this.DP = (DosageParameters)DP.Clone();
        this.SI = (SpecialInstructions)SI.Clone();
        this.RM = (ResourceModel)RM.Clone();
        this.type = im;
        this.property = p;
    }
    public MedicationDirection(int Med_ID, String Med_Name, String Granularity, DosageParameters DP, SpecialInstructions SI, int im, int p)
    {
        this.Med_ID = Med_ID;
        this.Med_Name = Med_Name;
        this.Granularity = Granularity;
        this.DP = (DosageParameters)DP.Clone();
        this.SI = (SpecialInstructions)SI.Clone();
        RM.resource = new int[Engine.DURATION];
        RM.processor = new int[Engine.DURATION];
        for (int i = 0; i < RM.processor.length; i++)
        {
            RM.resource[i] = 0;
            RM.processor[i] = 0;
        }
        this.type = im;
        this.property = p;
    }

    public Object Clone()
    {
        return new MedicationDirection(this.Med_ID, this.Med_Name, this.Granularity, this.DP, this.SI, this.RM, this.type,this.property);
    }
}
