package tw.edu.nctu.cs.pet.wedjat.core.Input;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.OMAT.ResourceModel;
import tw.edu.nctu.cs.pet.wedjat.core.startpoint.Engine;
import tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.Prescription;
import android.util.Log;

public class MedicineInput {
	
	public static Prescription.Builder prescription = Prescription.newBuilder();

    public List<MedicationDirection> getMSSFromFile(InputStream s)
    {
        
		BufferedReader reader = null;
		reader = new BufferedReader(new InputStreamReader(s));
        String tmp;
        
        //PrintWriter writer =new PrintWriter(new FileOutputStream("haha.txt"));
        //writer.println("asdsad");
        List<MedicationDirection> mss = new ArrayList<MedicationDirection>();
        int mnum = 0;
        
        try {
			while ((tmp = reader.readLine()) != null)
			{
				prescription.addDrugBuilder();
				prescription.getDrugBuilder(mnum).setId(mnum);
				
				Log.d("DEBUG",tmp);
				MedicationDirection temp = new MedicationDirection();
			    temp.RM = new ResourceModel();
			    temp.DP= new DosageParameters();
			    temp.SI = new SpecialInstructions();
			    temp.SI.Interferer = new ArrayList<Interferer>();
			    temp.SI.Interferer.clear();
			    temp.Med_ID = mnum;
			    mnum++;
			    temp.RM.MedID = temp.Med_ID;
			    String[] data = tmp.split(",");
			    temp.Med_Name = data[0];
			    temp.Granularity = data[1];
			    temp.DP.asmax = Integer.parseInt(data[2]); 
			    temp.DP.asmin = Integer.parseInt(data[3]);
			    temp.DP.nsmax = Integer.parseInt(data[4]); 
			    temp.DP.nsmin = Integer.parseInt(data[5]);
			    temp.DP.tmin = Integer.parseInt(data[6]);
			    temp.DP.tmax = Integer.parseInt(data[7]);
			    temp.DP.budget = Integer.parseInt(data[8]);
			    temp.DP.replenishment = Integer.parseInt(data[9]);
			    temp.DP.lowerbound = Integer.parseInt(data[10]);
			    temp.DP.pinterval = Integer.parseInt(data[11]);
			    temp.type = decideImportance(data[12]);
			    temp.property = decideProperty(data[13]);
			    int itmp=14;
			    if(data.length>14)
			    {
			        Interferer inter = new Interferer();
			        inter.MedID = Integer.parseInt(data[itmp]);
			        inter.minFrInterferer = Integer.parseInt(data[itmp+1]);
			        inter.minToInterferer = Integer.parseInt(data[itmp+2]);
			        temp.SI.Interferer.add(inter);
			        itmp += 3;
			    }
			    mss.add(temp);
			}
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			reader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return mss;
    }
    private int decideImportance(String s)
    {
    	int ss = Integer.parseInt(s);
        switch (ss)
        {
            case 1:
                return Importance.IMPORTANT;
            case 2:
                return Importance.ORDINARY;
            case 3:
                return Importance.NONNECESSARY;
            default:
               System.out.println("Importance Assign Wrong Number");
                return Importance.IMPORTANT;
        }
    }
    private  int decideProperty(String s)
    {
    	int ss = Integer.parseInt(s);
        switch (ss)
        {
            case 1:
                return Property.NORMAL;
            case 2:
                return Property.AFTERNEAL;
            case 3:
                return Property.BEFOREMEAL;
            default:
            	System.out.println("Property Assign Wrong Number");
                return Property.NORMAL;
        }
    }
    public static List<MedicationDirection> getMSS()
    { 
         ResourceModel rm = new ResourceModel();
        rm.processor = new int[Engine.DURATION];
        rm.resource = new int[Engine.DURATION];
        for (int i = 0; i < rm.processor.length; i++)
        {
            rm.resource[i] = 0;
            rm.processor[i] = 0;
        }
        rm.feasible = true;
        rm.schedule.clear();
        List<MedicationDirection> mss = new ArrayList<MedicationDirection>(); 
        MedicationDirection temp = new MedicationDirection();
        DosageParameters dp = new DosageParameters();
        SpecialInstructions si = new SpecialInstructions();
        Interferer inter = new Interferer();
        temp.RM = rm;
        si.Interferer.clear();
        temp.Med_ID = 0;
        temp.RM.MedID = temp.Med_ID;
        temp.Med_Name = "A";
        temp.Granularity = "tablet";
        dp.asmax =6; dp.asmin =4;
        dp.nsmax =6; dp.nsmin =4;
        dp.tmin = 240;
        dp.tmax = 240;
        dp.budget = 3;
        dp.replenishment = 24;
        dp.lowerbound = 3;
        dp.pinterval = 24;

        inter.MedID = 1;
        inter.minFrInterferer = 0;
        inter.minToInterferer = 2;
        si.Interferer.add((Interferer)inter.Clone());
        inter.MedID = 2;
        inter.minFrInterferer = 1;
        inter.minToInterferer = 0;
        si.Interferer.add((Interferer)inter.Clone());
        temp.DP = dp;
        temp.SI = si;
        temp.type = Importance.IMPORTANT;
        temp.property = Property.AFTERNEAL;
        mss.add((MedicationDirection)temp.Clone());

        si.Interferer.clear();
        temp.Med_ID = 1;
        temp.RM.MedID = temp.Med_ID;
        temp.Med_Name = "B";
        temp.Granularity = "tablet";
        dp.asmax = 7; dp.asmin =5;
        dp.nsmax = 7; dp.nsmin = 5;
        dp.tmin = 240;
        dp.tmax = 240;
        dp.budget = 3;
        dp.replenishment = 24;
        dp.lowerbound = 3;
        dp.pinterval = 24;
        temp.DP = dp;
        temp.SI = si;
        temp.type = Importance.NONNECESSARY;
        temp.property = Property.BEFOREMEAL;
        mss.add((MedicationDirection)temp.Clone());
        
        si.Interferer.clear();
        temp.Med_ID = 2;
        temp.RM.MedID = temp.Med_ID;
        temp.Med_Name = "C";
        temp.Granularity = "tablet";
        dp.asmax = 12; dp.asmin = 6;
        dp.nsmax =12; dp.nsmin = 6;
        dp.tmin = 240;
        dp.tmax = 240;
        dp.budget = 3;
        dp.replenishment = 24;
        dp.lowerbound = 3;
        dp.pinterval = 24;
        temp.DP = dp;
        temp.SI = si;
        temp.type = Importance.ORDINARY;
        temp.property = Property.NORMAL;
        mss.add((MedicationDirection)temp.Clone());

        return mss;
    }
}
