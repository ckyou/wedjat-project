package tw.edu.nctu.cs.pet.wedjat.core.Input;

import java.util.ArrayList;
import java.util.List;

public class SpecialInstructions {
	public List<Interferer> Interferer = new ArrayList<Interferer>();

    //ICloneable
    public SpecialInstructions() { }
    private SpecialInstructions(List<Interferer> Interferer)
    {
        for (Interferer i : Interferer)
            this.Interferer.add((Interferer)i.Clone());
    }
    public Object Clone()
    {
        return new SpecialInstructions(this.Interferer);
    }
}
