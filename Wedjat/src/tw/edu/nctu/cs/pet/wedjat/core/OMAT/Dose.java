package tw.edu.nctu.cs.pet.wedjat.core.OMAT;

public class Dose implements Cloneable, Comparable<Dose>{
	public int medID, start_time, end_time, dose_size;
    //ICloneable
    public Dose() { }
    public Dose(int medID, int time1, int time2, int dose_size)
    {
        this.medID = medID; this.start_time = time1; this.end_time = time2; this.dose_size = dose_size;
    }
    public Object Clone()
    {
        return new Dose(this.medID, this.start_time, this.end_time, this.dose_size);
    }

    //IComparable
    public boolean Equals(Dose other)
    {
        // this.comparer = this.time;
        if (this.start_time == other.start_time && this.end_time == other.end_time)
            return true;
        else
            return false;
    }
    
    @Override
	public int compareTo(Dose s) {
    	return new Integer(this.start_time).compareTo(s.start_time);
	}


}
