package tw.edu.nctu.cs.pet.wedjat.core.OMAT;

import java.util.ArrayList;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;

public class Job {
	public MedicationDirection medicine;
    public List<Dose> doses;
    public Job() { doses = new ArrayList<Dose>(); }
}
