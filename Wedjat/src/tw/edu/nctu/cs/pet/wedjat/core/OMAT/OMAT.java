package tw.edu.nctu.cs.pet.wedjat.core.OMAT;

import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.Input.Interferer;
import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;
import tw.edu.nctu.cs.pet.wedjat.core.startpoint.Engine;

public class OMAT {
	/*schedule -> Job -> dose
     * 輸入一個處方只會產生一個schedule
     * 在一個schedule裡面 每種藥都有一個Job => Schedule::List<Job>
     * 每個藥的Job裡面存著這個藥每次要吃的時間Dose => Job::List<Dose>
     */ 
    public Schedule run_omat(List<MedicationDirection> mss)
    {

        Schedule schedule = new Schedule(mss.size());
        for (int i = 0; i < mss.size(); ++i)
        {
            schedule.jobs.get(i).medicine = mss.get(i);
            
            int mid = (mss.get(i).DP.nsmin + mss.get(i).DP.nsmax) / 2;
            int half_interval = mid - mss.get(i).DP.nsmin;
            int s;
            for (s = 0; s < mss.get(i).RM.resource.length; ++s)
            {
                if (mss.get(i).RM.resource[s] == 0) break;  //找到第一個可用的resource
            }
            if (s == mss.get(i).RM.resource.length)
            {
                //Console.WriteLine(mss.get(i).Med_Name + " No Schedule\r\n");
                schedule.jobs.get(i).medicine.RM.feasible=false;   // 沒有resource wrong！
            }
            int start;
            int end = mss.get(i).RM.processor.length;
            start = (s - half_interval) < 0 ? 0 : s - half_interval;
            schedule.jobs.get(i).doses.add(new Dose(mss.get(i).Med_ID, start, s + half_interval, 1));
            for (start = s + mid; start < end; )
            {
                int dend = (start + half_interval) > Engine.DURATION ? Engine.DURATION : start + half_interval;
                int dstart = (start - half_interval) > Engine.DURATION ? Engine.DURATION : start - half_interval;
                
                Dose d = new Dose(mss.get(i).Med_ID, dstart, dend, 1);
                schedule.jobs.get(i).doses.add(d);
                start += mid;
            }

            setResource(mss, schedule.jobs.get(i));
            removeConflict(mss.get(i), schedule.jobs.get(i));

        }

        return schedule;
    }




    private void removeConflict(MedicationDirection m, Job job)
    {
        for (Dose d : job.doses)
        {
            int i, bstart = d.start_time, bend = d.end_time;
            for (i = d.start_time; i <= d.end_time; ++i)
            {
                if (m.RM.resource[i] == 1)
                {
                    bstart = i;
                    break;
                }
            }
            if (i > d.end_time) continue ;
            for (; i <= d.end_time; ++i)
            {
                if (m.RM.resource[i] == 0)
                {
                    bend = i;
                    break;
                }
            }
            if (i > d.end_time) bend = d.end_time;

            if ((bstart == d.start_time && bend == d.end_time) || (bstart != d.start_time && bend != d.end_time))
            {
                    //Console.WriteLine(job.medicine.Med_Name + " No Schedule at "+ d.start_time+" "+d.end_time);
                    break;
            }
           
            
            if (bend == d.end_time) d.end_time = bstart;
            else if (bstart == d.start_time) d.start_time = bend;
        }
    }

    private void setResource(List<MedicationDirection> mss, Job job)
    {
        for (Interferer inter : job.medicine.SI.Interferer)
        {
            for (Dose d : job.doses)
            {
                int mid = ((d.start_time + d.end_time) / 2);
                int start = (mid < inter.minFrInterferer) ? 0 : mid - inter.minFrInterferer;
                int end = (mid + inter.minToInterferer > Engine.DURATION) ? Engine.DURATION : mid + inter.minToInterferer;
                for (int i = start; i < end; ++i)
                    mss.get(inter.MedID).RM.resource[i] = 1;
            }
        }
    }
    
}
