package tw.edu.nctu.cs.pet.wedjat.core.OMAT;

import java.util.ArrayList;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.startpoint.Engine;

public class ResourceModel {
	public int MedID;
    public int[] resource;
    public int[] processor;
    public boolean feasible = true;
    public List<Integer> schedule = new ArrayList<Integer>();
    //public int priority = 0;

    public ResourceModel() {
        processor = new int[Engine.DURATION];
        resource = new int[Engine.DURATION];
        for (int i = 0; i < processor.length; i++)
        {
            resource[i] = 0;
            processor[i] = 0;
        }
    }

    private ResourceModel(int MedID, int[] resource, int[] processor, boolean feasible, List<Integer> schedule)
    {
        this.MedID = MedID;
        this.processor = processor;
        this.resource = resource;
        this.feasible = feasible;
        for (int i : schedule)
            this.schedule.add(i);

        for (int i = 0; i < processor.length; i++)
        {
            this.resource[i] = resource[i];
            this.processor[i] = processor[i];
        }
    }

    public Object Clone()
    {
        return new ResourceModel(this.MedID, this.resource, this.processor, this.feasible, this.schedule);
    }
}
