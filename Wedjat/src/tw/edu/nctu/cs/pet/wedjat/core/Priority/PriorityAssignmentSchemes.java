package tw.edu.nctu.cs.pet.wedjat.core.Priority;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;

public class PriorityAssignmentSchemes {
	public List<Priority_Order> RM(List<MedicationDirection> list_mss)
    {
        List<Priority_Order> list_priority = new ArrayList<Priority_Order>();
        Priority_Order temp = new Priority_Order();
        for (int i = 0; i < list_mss.size(); i++)
        {
            temp.medID = list_mss.get(i).Med_ID; temp.comparer = list_mss.get(i).DP.asmax;
            list_priority.add((Priority_Order)temp.Clone());
        }
        Collections.sort(list_priority);
        return list_priority;
    }
    public List<Priority_Order> MVF(List<MedicationDirection> list_mss)
    {
        List<Priority_Order> list_priority = new ArrayList<Priority_Order>();
        Priority_Order temp = new Priority_Order();
        for (int i = 0; i < list_mss.size(); i++)
        {
            temp.medID = list_mss.get(i).Med_ID;
            if (list_mss.get(i).SI.Interferer.size() == 0) temp.comparer = 0;
            else
            {
                temp.comparer = list_mss.get(i).SI.Interferer.get(0).minFrInterferer;
                //Find maximum blocking time
                for (int j = 1; j < list_mss.get(i).SI.Interferer.size(); j++)
                {
                    if (temp.comparer < list_mss.get(i).SI.Interferer.get(j).minFrInterferer)
                        temp.comparer = list_mss.get(i).SI.Interferer.get(j).minFrInterferer;
                }
            }
            list_priority.add((Priority_Order)temp.Clone());
        }
        Collections.sort(list_priority);
        Collections.reverse(list_priority);

        return list_priority;
    }
    public List<Priority_Order> MIF(List<MedicationDirection> list_mss)
    {
        List<Priority_Order> list_priority = new ArrayList<Priority_Order>();
        Priority_Order temp = new Priority_Order();
        for (int i = 0; i < list_mss.size(); i++)
        {
            temp.medID = list_mss.get(i).Med_ID;
            for (int j = 0; j < list_mss.get(i).SI.Interferer.size(); j++)
            {
                if (list_mss.get(i).SI.Interferer.get(j).minFrInterferer > 0)
                    temp.comparer++;
            }
            list_priority.add((Priority_Order)temp.Clone());
        }
        Collections.sort(list_priority); Collections.reverse(list_priority);
        return list_priority;
    }
    public List<Priority_Order> SSDF(List<MedicationDirection> list_mss)
    {
        List<Priority_Order> list_priority = new ArrayList<Priority_Order>();
        Priority_Order temp = new Priority_Order();
        for (int i = 0; i < list_mss.size(); i++)
        {
            temp.medID = list_mss.get(i).Med_ID; temp.comparer = list_mss.get(i).DP.nsmax - list_mss.get(i).DP.nsmin;
            list_priority.add((Priority_Order)temp.Clone());
        }
        Collections.sort(list_priority);
        return list_priority;
    }
    /*public List<Priority_Order> EDF(ref List<JobModel> list_jm)
    {
        List<Priority_Order> list_priority = new List<Priority_Order>();
        Priority_Order temp = new Priority_Order();
        for (int i = 0; i < list_jm.Count; i++)
        {
            temp.medID = list_jm[i].MedID; temp.comparer = list_jm[i].deadline;
            list_priority.Add((Priority_Order)temp.Clone());
        }
        list_priority.Sort();
        return list_priority;
    }*/
}


