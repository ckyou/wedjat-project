package tw.edu.nctu.cs.pet.wedjat.core.Priority;

public interface PrioritySchemes {
	public static final int RM=0;
	public static final int MVF=1;
	public static final int MIF=2;
	public static final int SSDF=3;
	public static final int EDF=4;
}
