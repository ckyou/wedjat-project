package tw.edu.nctu.cs.pet.wedjat.core.Priority;

public class Priority_Order implements Cloneable,Comparable<Priority_Order>{
	public int medID, comparer;
    //IComparable
    public Boolean Equals(Priority_Order other)
    {
        if (this.comparer == other.comparer)
            return true;
        else
            return false;
    }
 
    //ICloneable
    public Priority_Order() { }
    private Priority_Order(int medID, int comparer)
    {
        this.medID = medID; this.comparer = comparer;
    }
    public Object Clone()
    {
        return new Priority_Order(this.medID, this.comparer);
    }

	public int compareTo(Priority_Order RM) {
		// TODO Auto-generated method stub
		return new Integer(this.comparer).compareTo(RM.comparer);
	}
}
