package tw.edu.nctu.cs.pet.wedjat.core.startpoint;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.core.Group.Cluster;
import tw.edu.nctu.cs.pet.wedjat.core.Group.GroupDose;
import tw.edu.nctu.cs.pet.wedjat.core.Group.Grouping;
import tw.edu.nctu.cs.pet.wedjat.core.Group.OMATtoGroup;
import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicationDirection;
import tw.edu.nctu.cs.pet.wedjat.core.Input.MedicineInput;
import tw.edu.nctu.cs.pet.wedjat.core.OMAT.Dose;
import tw.edu.nctu.cs.pet.wedjat.core.OMAT.Job;
import tw.edu.nctu.cs.pet.wedjat.core.OMAT.OMAT;
import tw.edu.nctu.cs.pet.wedjat.core.OMAT.Schedule;
import tw.edu.nctu.cs.pet.wedjat.core.Priority.PriorityAssignmentSchemes;
import tw.edu.nctu.cs.pet.wedjat.core.Priority.PrioritySchemes;
import tw.edu.nctu.cs.pet.wedjat.core.Priority.Priority_Order;
import tw.edu.nctu.cs.pet.wedjat.utility.Config;
import tw.nctu.cs.pet.wedjat.R;
import android.content.Context;
import android.content.res.Resources;
import android.os.Environment;
import android.widget.Toast;

public class Engine {
  
	public static File path;
    public static int DURATION=24*7;  //總共排藥時間
    
    
    public static int item[][];
    public static int item_Count[];

  
  //程式開始點
    public static void run(Context context)
    {
        
        /*
         *    主程式流程  輸入藥物-> 指定Priority -> Run OMAT  ->  Grouping
         *    請先看此份code
         */
         
        //  First Step
        MedicineInput mi=new MedicineInput();
        //List<MedicationDirection> tmp = mi.getMSSFromFile("..\\..\\SelfTestData\\easy.txt");  //輸入所有藥物
    	String state = Environment.getExternalStorageState();
    	String pathForRead;
    	/*if (Environment.MEDIA_MOUNTED.equals(state)) {
    	    //可以對media作讀寫
    		File path=this.getExternalFilesDir(null);
    		pathForRead = path.toString();
    		pathForRead += "/SelfTestData/easy.txt";*/
    		Resources resources=context.getResources();
    		InputStream medicine=resources.openRawResource(R.raw.easy);
    		List<MedicationDirection> tmp = mi.getMSSFromFile(medicine);  //輸入所有藥物
    		/*File file=new File(path,"test.txt");
    		PrintWriter writer = null;
    		try {
    			writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file)));
    		} catch (FileNotFoundException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
    		if(writer==null)
    			Log.d("TEST","writer = null");
    		if(file==null)
    			Log.d("TEST","file = null");
    		
    		writer.println("I am in "+file.getAbsolutePath());
            Log.d("TEST","I am in "+file.getAbsolutePath());
            writer.close(); */
    	/*} else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
    	    //只可以對media作讀取
    	} else {
    	    //無法作讀寫
    	}*/
    	/*File file=new File("test.txt");
        PrintWriter writer = null;
		try {
			writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        writer.println("I am in "+file.getAbsolutePath());
        Log.d("TEST","I am in "+file.getAbsolutePath());
        writer.close(); */
    		
        //  Second Step
        PriorityAssignmentSchemes pas = new PriorityAssignmentSchemes();
        List<Priority_Order> list_priority = new ArrayList<Priority_Order>();
        /*
         * Decide Priority
         */
        int ps = PrioritySchemes.RM; //decide priority

        switch (ps)
        {
            //case PrioritySchemes.EDF: list_priority = pas.EDF(ref list_jm); break;  // EDF是ODAT在用的  暫時拿掉
            case PrioritySchemes.MIF: list_priority = pas.MIF(tmp); break;
            case PrioritySchemes.MVF: list_priority = pas.MVF(tmp); break;
            case PrioritySchemes.RM: list_priority = pas.RM(tmp); break;
            case PrioritySchemes.SSDF: list_priority = pas.SSDF(tmp); break;
        }               //assign Priority

        List<MedicationDirection> mss = new ArrayList<MedicationDirection>();
        for (Priority_Order po : list_priority)
        {
            mss.add(tmp.get(po.medID));   //把照PRORITY算出來的List<Priority_Order>  list_priority轉為MSS格式List<MedicationDirection> mss
        }


        //Third Step
        OMAT omat = new OMAT();
        Schedule schedule = omat.run_omat(mss);   //Run OMAT


        /*OutputStreamWriter write;
        write = File.CreateText("..\\..\\Result\\OMATResult.txt"); //??
        write.close();
        write = File.AppendText("..\\..\\Result\\OMATResult.txt"); //??*/


//        	path=context.getExternalFilesDir(null);
//	    	pathForRead = path.toString();
//	    	pathForRead += "/OMATResult.txt";
//	    	File OMATResult = new File(pathForRead); 
        
        
        	path=new File(Environment.getExternalStorageDirectory().getAbsolutePath()+Config.scheduleFolder);
        	if(!path.exists()){
        		path.mkdirs();
        		Toast.makeText(context, "make", Toast.LENGTH_LONG).show();
        	}
        	
        	File OMATResult = new File(path.getAbsolutePath(), "OMATResult.txt");
        	
        	Toast.makeText(context, path.getAbsolutePath(), Toast.LENGTH_LONG).show();
        

        FileWriter fwriter;
		try {
			fwriter = new FileWriter(OMATResult);
		
	        int nr=0;
	        for (Job j : schedule.jobs)
	        {
	            for (Dose d : j.doses)
	            {
	                fwriter.write(j.medicine.Med_Name + "    " + d.start_time + "    " + d.end_time+"\r\n");
	                ++nr;
	            }
	        }  //印出OMAT結果
	
	        fwriter.write("\r\n共" + nr + "筆\r\n");
	        fwriter.close();
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



        //Fourth Step 

        OMATtoGroup glue = new OMATtoGroup();
        ArrayList<GroupDose> DoseList = glue.genGroupDoseList(schedule);  //把OMAT的結果轉成Group的格式

        Grouping group = new Grouping();
        Cluster cluster = group.group_start(DoseList);

        /*GenerateEvent generateEvent = new GenerateEvent(cluster);
        List<Event> EventList= generateEvent.genFirstAlarm(tmp);

        Alarmer alarm = new Alarmer();
        alarm.genAlarm(EventList);*/




        
     }
}