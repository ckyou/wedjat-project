package tw.edu.nctu.cs.pet.wedjat.gpb;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.Prescription;

/**
 * ProtoFileParse is used to help developer to parse our .proto file, and 
 * return value is the data object
 * 
 * @author CKYOU
 *
 */
public class ProtoFileParser {
	private ProtoFileParser(){
		
	}
	
	public static Prescription from(String path){
		Prescription prescription = null;
		try {
			prescription = Prescription.parseFrom(new FileInputStream(path));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prescription;
	}
	
}
