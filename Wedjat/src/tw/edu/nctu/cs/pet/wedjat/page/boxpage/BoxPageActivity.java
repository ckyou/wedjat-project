package tw.edu.nctu.cs.pet.wedjat.page.boxpage;

import java.util.ArrayList;

import tw.edu.nctu.cs.pet.wedjat.page.boxpage.BoxView.SlotTag;
import tw.edu.nctu.cs.pet.wedjat.page.listpage.ListPageActivity;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.PillBox;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.ActionSlot;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.BaseSlot;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.DrugSlot;
import tw.nctu.cs.pet.wedjat.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;

/**
 * 
 * @author CKYOU
 */
public class BoxPageActivity extends Activity{
	private final String TAG = "BoxActivity";
	private Context context;
	private BoxView box;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_boxpage);
		
		context = this;
		
		boolean state = buildBox(this);
		
		FrameLayout wrapper = (FrameLayout) findViewById(R.id.wrapper);
		if(state){
			wrapper.addView(box);
		}else{
			LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			wrapper.addView(inflater.inflate(R.layout.error, null));
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private boolean buildBox(Context context){
		PillBox pbox = PillBox.getInstance();
		if(pbox==null) 
			return false; 
		
		box = new BoxView(context);
		
		for(int i=0; i<7; ++i){
			ArrayList<BaseSlot> slots = pbox.WEEK.get(i);
			for(int j=0; j<slots.size(); ++j){
				if( slots.get(j) instanceof DrugSlot){
					box.addDrugSlot(context, i, j, boxClickListener);
				}else if(slots.get(j) instanceof ActionSlot){
					//TODO: 
				}else{
					//ERROR
					return false;
				}
			}
		}
		
		return true;
	}
	
	OnClickListener boxClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			BoxView.SlotTag tag = (SlotTag) v.getTag();
			
			if(tag==null){
				Toast.makeText(context, "click doesn't work due to empty tag", Toast.LENGTH_SHORT).show();
			}else{
				Intent goToListPage = new Intent();
				goToListPage.setClass(context, ListPageActivity.class);
				goToListPage.putExtra("day", tag.day);
				goToListPage.putExtra("index", tag.index);
				startActivity(goToListPage);
			}
			
		}
	};
	
}
