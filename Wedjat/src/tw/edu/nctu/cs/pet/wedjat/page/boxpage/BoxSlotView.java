package tw.edu.nctu.cs.pet.wedjat.page.boxpage;

import tw.nctu.cs.pet.wedjat.R;
import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BoxSlotView extends FrameLayout {
	public static final int TYPE_DRUGSLOT = 0;
	public static final int TYPE_ACTIONSLOT = 0;

	public static final int BADGE_POSITION_TOP_LEFT = 1;
	public static final int BADGE_POSITION_TOP_RIGHT = 2;
	public static final int BADGE_POSITION_BOTTOM_LEFT = 3;
	public static final int BADGE_POSITION_BOTTOM_RIGHT = 4;
	public static final int BADGE_POSITION_CENTER = 5;

	private Button btn;
	private int badgeMarginH;
	private int badgeMarginV;

	public BoxSlotView(Context context, int type) {
		super(context);

		LayoutParams params = new LayoutParams(
		        LayoutParams.MATCH_PARENT,      
		        LayoutParams.MATCH_PARENT
		);
		
		btn = new Button(context);
		btn.setBackgroundResource(R.drawable.selector_boxslot_background_normal);
		btn.setMinHeight(dipToPixels(100));
		
		//TODO: make this setting dynamically and flexibly
		params.setMargins(dipToPixels(5), dipToPixels(5), dipToPixels(5), dipToPixels(5));
		btn.setLayoutParams(params);
		
		this.addView(btn);
	}
	
	public void setText(String text){
		btn.setText(text);
	}
		
	public void setHeight(int pixels){
		btn.setHeight(pixels);
	}
	
	public void setWidth(int pixels){
		btn.setWidth(pixels);
	}
		
	public void setBackgroundResource(int resId){
		btn.setBackgroundResource(resId);
		invalidate();
	}
	
	public void setOnClickListener(OnClickListener listener){
		btn.setOnClickListener(listener);
	}
	
	public void setTag(Object tag){
		btn.setTag(tag);
	}
	
	public Object getTag(){
		return btn.getTag();
	}

	public void setBadge(View view, int position) {
		LayoutParams lp = new FrameLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

		badgeMarginH = dipToPixels(10);
		badgeMarginV = badgeMarginH;

		switch (position) {
		case BADGE_POSITION_TOP_LEFT:
			lp.gravity = Gravity.LEFT | Gravity.TOP;
			lp.setMargins(badgeMarginH, badgeMarginV, 0, 0);
			break;
		case BADGE_POSITION_TOP_RIGHT:
			lp.gravity = Gravity.RIGHT | Gravity.TOP;
			lp.setMargins(0, badgeMarginV, badgeMarginH, 0);
			break;
		case BADGE_POSITION_BOTTOM_LEFT:
			lp.gravity = Gravity.LEFT | Gravity.BOTTOM;
			lp.setMargins(badgeMarginH, 0, 0, badgeMarginV);
			break;
		case BADGE_POSITION_BOTTOM_RIGHT:
			lp.gravity = Gravity.RIGHT | Gravity.BOTTOM;
			lp.setMargins(0, 0, badgeMarginH, badgeMarginV);
			break;
		case BADGE_POSITION_CENTER:
			lp.gravity = Gravity.CENTER;
			lp.setMargins(0, 0, 0, 0);
			break;
		default:
			break;
		}

		view.setLayoutParams(lp);
		this.addView(view);
	}

	private int dipToPixels(int dip) {
		Resources r = getResources();
		float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip,
				r.getDisplayMetrics());
		return (int) px;
	}
}
