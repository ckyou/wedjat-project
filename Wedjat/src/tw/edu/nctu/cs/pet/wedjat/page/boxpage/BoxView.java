package tw.edu.nctu.cs.pet.wedjat.page.boxpage;

import java.util.ArrayList;

import tw.edu.nctu.cs.pet.wedjat.utility.Config;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.PillBox;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.DrugSlot;
import tw.nctu.cs.pet.wedjat.R;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;


public class BoxView extends FrameLayout{

	private String TAG = "BoxView";
	protected ScrollableTableView table;
	private LinearLayout week, header;
	private ArrayList<LinearLayout> cols;
	private ArrayList<TextView> hCols;
	
	private int colWidth, rowHeight;

	public BoxView(Context context) {
		super(context);
				
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(dm);

		colWidth = dm.widthPixels/4;
		rowHeight = dm.heightPixels/3;
		
		table = new ScrollableTableView(context);
		this.addView(table);
		Log.d(TAG, "init STable");
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		week = (LinearLayout) inflater.inflate(R.layout.boxview_content, null);
		header = (LinearLayout) inflater.inflate(R.layout.boxview_header, null);
		
		cols = new ArrayList<LinearLayout>();
		for(int i=0; i<7; ++i){
			cols.add((LinearLayout) week.findViewById(
						context.getResources().getIdentifier("col"+i, "id", context.getPackageName())
					));
		}
		
		hCols = new ArrayList<TextView>();
		String[] dayAbbr = getResources().getStringArray(R.array.days_abbreviation);
		for(int i=0; i<7; ++i){
			hCols.add((TextView) header.findViewById(
						context.getResources().getIdentifier("h"+i, "id", context.getPackageName())
					));
			//set Text
			hCols.get(i).setText(dayAbbr[i]);
		}
				
		
		table.addContent(week);
		table.addHeader(header);
	}
	
	public boolean addDrugSlot(Context context, int day, int index, OnClickListener listener){
		PillBox pbox = PillBox.getInstance();
		if(pbox==null) 
			return false; 
		
		BoxSlotView b = new BoxSlotView(context, BoxSlotView.BADGE_POSITION_BOTTOM_LEFT);
		
		b.setHeight(rowHeight);
		b.setText(pbox.WEEK.get(day).get(index).startTime
		+"\n|\n"+
		pbox.WEEK.get(day).get(index).endTime
		);
		
		if(pbox.WEEK.get(day).get(index) instanceof DrugSlot){
			//set badge
			TextView num = new TextView(context);
			num.setText(""+((DrugSlot)pbox.WEEK.get(day).get(index)).drugList.size());
			b.setBadge(num, BoxSlotView.BADGE_POSITION_BOTTOM_LEFT);
		}
		
		b.setOnClickListener(listener);
		b.setTag(new SlotTag(day, index));
		cols.get(day).addView(b);
		return true;
	}
	
	public class SlotTag{
		public int day, index;
		public SlotTag(int day, int index){
			this.day = day;
			this.index = index;
		}
	}
}
