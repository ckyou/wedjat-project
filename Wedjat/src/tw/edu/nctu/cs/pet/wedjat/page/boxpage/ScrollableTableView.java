package tw.edu.nctu.cs.pet.wedjat.page.boxpage;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

/**
 * This layout provide a two dimension ScrollView with a static header on the top of screen<br>
 * ans this header will be scrolled along with content inside the 2D ScrollView(called content)<br><br>
 * <b> To guarantee the best appearance, you must ensure the width of column are identical in header and content
 * @author CKYOU
 */
public class ScrollableTableView extends FrameLayout {
	private final String TAG = "ScrollableTableView";

	private VScroll vScrollContent;
	private HScroll hScrollContent, hScrollHeader;

	LinearLayout container, header;

	float mx, my;
	float curX, curY;
	
	public void addContent(LinearLayout child) {
		hScrollContent.addView(child);
	}
	
	public void addHeader(View child) {
		hScrollHeader.addView(child);
	}
	
	public void setFadingEdgeEnabled(boolean horizonSide, boolean verticalSide){
		hScrollContent.setHorizontalFadingEdgeEnabled(horizonSide);
		vScrollContent.setVerticalFadingEdgeEnabled(verticalSide);
	}
	
	/*
	 *
	 */
	
	private void initSTView(Context context) {		
		//init header layout
		hScrollHeader.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT));
		hScrollHeader.setHorizontalScrollBarEnabled(false);
		
		//init content layout
		hScrollContent.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		vScrollContent.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));

		vScrollContent.addView(hScrollContent);
		
		//init outer container
		container.setOrientation(LinearLayout.VERTICAL);
		container.addView(hScrollHeader);
		container.addView(vScrollContent);
		super.addView(container);
	}
	
	
	public ScrollableTableView(Context context) {
		super(context);
		vScrollContent = new VScroll(context);
		hScrollContent = new HScroll(context);
		hScrollHeader = new HScroll(context);
		container = new LinearLayout(context);
		header = new LinearLayout(context);
		initSTView(context);
	}

	public ScrollableTableView(Context context, AttributeSet attrs) {
		super(context, attrs);
		vScrollContent = new VScroll(context, attrs);
		hScrollContent = new HScroll(context, attrs);
		hScrollHeader = new HScroll(context, attrs);
		container = new LinearLayout(context, attrs);
		header = new LinearLayout(context, attrs);
		initSTView(context);
	}

	public ScrollableTableView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		vScrollContent = new VScroll(context, attrs, defStyle);
		hScrollContent = new HScroll(context, attrs, defStyle);
		hScrollHeader = new HScroll(context, attrs, defStyle);
		container = new LinearLayout(context, attrs);
		header = new LinearLayout(context, attrs);
		// need API 11
		// container = new LinearLayout(context, attrs, defStyle);
		initSTView(context);
	}
	
	@Override
	public void addView(View child, int width, int height) {
		if (getChildCount() > 0)
			addViewException();
		else
			super.addView(child, width, height);
	}

	@Override
	public void addView(View child, int index,
			android.view.ViewGroup.LayoutParams params) {
		if (getChildCount() > 0)
			addViewException();
		else
			super.addView(child, index, params);
	}

	@Override
	public void addView(View child, int index) {
		if (getChildCount() > 0)
			addViewException();
		else
			super.addView(child, index);
	}

	@Override
	public void addView(View child, android.view.ViewGroup.LayoutParams params) {
		if (getChildCount() > 0)
			addViewException();
		else
			super.addView(child, params);
	}

	@Override
	public void addView(View child) {
		if (getChildCount() > 0)
			addViewException();
		else
			super.addView(child);
	}

	private void addViewException() {
		throw new IllegalStateException(TAG
				+ "can NOT add view directly by this way");
	}
	
	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
		/*
		 * Override onInterceptTouchEvent() to prevent the situation that
		 * component(ex. Button) inside container affect scrollView not
		 * scrolling
		 */

		// keep record press down position
		if (ev.getAction() == MotionEvent.ACTION_DOWN) {
			mx = ev.getX();
			my = ev.getY();
		}

		if (ev.getAction() == MotionEvent.ACTION_MOVE) {
			return this.onTouchEvent(ev);
		}

		return super.onInterceptTouchEvent(ev);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			mx = event.getX();
			my = event.getY();
			break;
		case MotionEvent.ACTION_MOVE:
			curX = event.getX();
			curY = event.getY();
			vScrollContent.scrollBy((int) (mx - curX), (int) (my - curY));
			hScrollContent.scrollBy((int) (mx - curX), (int) (my - curY));
			hScrollHeader.scrollBy((int) (mx - curX), (int) (my - curY));
			mx = curX;
			my = curY;
			break;
		case MotionEvent.ACTION_UP:
			curX = event.getX();
			curY = event.getY();
			vScrollContent.scrollBy((int) (mx - curX), (int) (my - curY));
			hScrollContent.scrollBy((int) (mx - curX), (int) (my - curY));
			hScrollHeader.scrollBy((int) (mx - curX), (int) (my - curY));
			break;
		}

		return true;
	}

	private class VScroll extends ScrollView {

		private int mFadeColor = -1;
		
		public VScroll(Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
		}

		public VScroll(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		public VScroll(Context context) {
			super(context);
		}

		@Override
		public boolean onTouchEvent(MotionEvent ev) {
			return false;
		}

		@Override
		public int getSolidColor() {
			if(mFadeColor!=-1)
				return mFadeColor;
			else 
				return super.getSolidColor();
		}

		public void setFadeColor(int fadeColor) {
			mFadeColor = fadeColor;
		}

		public int getFadeColor() {
			return mFadeColor;
		}
	}

	private class HScroll extends HorizontalScrollView {

		private int mFadeColor = -1;
		
		public HScroll(Context context, AttributeSet attrs, int defStyle) {
			super(context, attrs, defStyle);
		}

		public HScroll(Context context, AttributeSet attrs) {
			super(context, attrs);
		}

		public HScroll(Context context) {
			super(context);
		}

		@Override
		public boolean onTouchEvent(MotionEvent ev) {
			return false;
		}
		
		@Override
		public int getSolidColor() {
			if(mFadeColor!=-1)
				return mFadeColor;
			else 
				return super.getSolidColor();
		}

		public void setFadeColor(int fadeColor) {
			mFadeColor = fadeColor;
		}

		public int getFadeColor() {
			return mFadeColor;
		}
	}
}
