package tw.edu.nctu.cs.pet.wedjat.page.entrance;

import java.io.File;

import tw.edu.nctu.cs.pet.wedjat.core.startpoint.Engine;
import tw.edu.nctu.cs.pet.wedjat.page.boxpage.BoxPageActivity;
import tw.edu.nctu.cs.pet.wedjat.page.listpage.ListPageActivity;
import tw.edu.nctu.cs.pet.wedjat.test.TestAlarmServiceActivity;
import tw.edu.nctu.cs.pet.wedjat.test.TestBoxActivity;
import tw.edu.nctu.cs.pet.wedjat.utility.Config;
import tw.edu.nctu.cs.pet.wedjat.utility.Tool;
import tw.nctu.cs.pet.wedjat.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;

/**
 * <br>EntranceActivity is designed to be called up when user startup Wedjat
 * <br><b>In this activity, will do some check and show splash animation...etc</b>
 * <ul>NOW this activity will do: 
 * <li>check schedule is generated or not 
 * </ul>
 * 
 * @author CKYOU
 * @version 2013/07/20 updated
 */
public class EntranceActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		/** check the storage **/
		//TODO: this part is not complete
		Environment.getExternalStorageState();
		
		/** Check schedule gbp file 
		 *  if no schedule file exist, means this user run this program first time
		 *  EntranceActivity need to start activity to get user's prescription
		 *  
		 *  TODO:
		 *  IN FACT, should make Engine class to get real user's prescription as its input
		 * **/
		if(!Tool.schuduleIsExist()) 
			Engine.run(this);

		Intent intent = new Intent();
		intent.setClass(this, BoxPageActivity.class);
		startActivity(intent);
		finish();
	}
		
}
