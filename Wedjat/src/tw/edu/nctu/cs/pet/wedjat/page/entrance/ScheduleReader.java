package tw.edu.nctu.cs.pet.wedjat.page.entrance;

import java.util.ArrayList;
import java.util.List;

import tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.Prescription;
import tw.edu.nctu.cs.pet.wedjat.gpb.ProtoFileParser;
import tw.edu.nctu.cs.pet.wedjat.utility.Config;
import tw.edu.nctu.cs.pet.wedjat.utility.Tool;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Drug;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.PillBox;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot;
import android.os.Environment;
import android.util.Log;

public class ScheduleReader {
	
	private final static String TAG = "ScheduleReader";
	
	private ScheduleReader() {
	}
	
	public static boolean fromProtobufFile(){
		//check the schulde is exist or not
		if(!Tool.schuduleIsExist()) 
			return false;
		
		PillBox pbox = PillBox.getNewInstance();
		
		Prescription pscrip = ProtoFileParser.from(
				Environment.getExternalStorageDirectory().getAbsolutePath()
				+ Config.scheduleFolder + Config.scheduleName);
		
		List<tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.Drug> drugList = pscrip.getDrugList();
		List<tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.Cluster> clusterList = pscrip.getClusterList();
		List<tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.TimeSlot> timeSlotList = pscrip.getTimeSlotList();
		
		tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.Group bestGroup;
		tw.edu.nctu.cs.pet.wedjat.gpb.Prescriptions.TimeSlot bestTimeSlot;
		
//		Log.d("DDDDD", "size "+drugList.size());
//		for(int i=0; i<drugList.size(); ++i){
//			Log.d("DDDDD", ""+drugList.get(i).getId());
//		}
		
        //TODO: Prescriptions proto does not include the schedule time-range
        // 		hardcoded here definitely not a good implement way
        int base = tw.edu.nctu.cs.pet.wedjat.core.startpoint.Engine.DURATION/24;	
		int day=0;
		ArrayList<Drug> list;
		for(int i=0; i<clusterList.size() && day<7; ++i){
			bestGroup = clusterList.get(i).getGroup(0);
			bestTimeSlot = timeSlotList.get(bestGroup.getTimeSlotId(0));;
									
			if(bestTimeSlot.getEndTime() > (day+1)*24){
				++day;
			}
			
			list = new ArrayList<Drug>();
			for(int z=0; z<bestGroup.getDoseIdCount(); ++z){
				int acc=0;
				for(int d=0; d<drugList.size(); ++d)
				{	
					acc += drugList.get(d).getDoseCount();
					if(acc > bestGroup.getDoseId(z)){
						//TODO: the drug info including: name, dosage should be pull from database 
						//		or maybe this pulling action should be done at ListPage
						list.add(new Drug(""+d, "info in db"));
						break;
					}
				}				
			}

			if(day > 0 ){
				pbox.WEEK.get(day).add(new Slot.DrugSlot(
						bestTimeSlot.getStartTime() % (day*24) , 
						bestTimeSlot.getEndTime()  % (day*24), 
						list));
			}else{
				pbox.WEEK.get(day).add(new Slot.DrugSlot(
						bestTimeSlot.getStartTime(), 
						bestTimeSlot.getEndTime(), 
						list));
			}

		}

		
		Log.d(TAG, "set pillbox fromProtobufFile successfully");
		return true;
	}
	
	public static boolean fromDebugSchedule(){
		PillBox pbox = PillBox.getInstance();
		pbox.clear();
		
		ArrayList<Drug> list = new ArrayList<Drug>();
        list.add(new Drug("id0000", "20mg"));
        list.add(new Drug("id0008", "120ml"));
        list.add(new Drug("id0010", "20mg"));
        list.add(new Drug("id0018", "120ml"));
		
        pbox.ColMON.add(new Slot.DrugSlot(1200, 1400, list));
        pbox.ColMON.add(new Slot.DrugSlot(1600, 1800, list));
        pbox.ColTUE.add(new Slot.DrugSlot(1200, 1400, list));
        pbox.ColTUE.add(new Slot.DrugSlot(1600, 1800, list));
        pbox.ColTUE.add(new Slot.DrugSlot(2000, 2100, list));
        pbox.ColWED.add(new Slot.DrugSlot(1400, 1600, list));
        pbox.ColWED.add(new Slot.DrugSlot(2000, 2100, list));
        pbox.ColTHR.add(new Slot.DrugSlot(1200, 1400, list));
        pbox.ColTHR.add(new Slot.DrugSlot(1600, 1800, list));
        pbox.ColFRI.add(new Slot.DrugSlot(1600, 1800, list));
        pbox.ColSAT.add(new Slot.DrugSlot(1200, 1400, list));
        pbox.ColSAT.add(new Slot.DrugSlot(1600, 1800, list));
        pbox.ColSAT.add(new Slot.DrugSlot(2000, 2100, list));
        pbox.ColSUN.add(new Slot.DrugSlot(1200, 1400, list));
        pbox.ColSUN.add(new Slot.DrugSlot(1600, 1800, list));
        
        return true;
	}
	
}
