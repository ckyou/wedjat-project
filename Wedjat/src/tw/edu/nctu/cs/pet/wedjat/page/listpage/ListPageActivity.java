package tw.edu.nctu.cs.pet.wedjat.page.listpage;

import tw.edu.nctu.cs.pet.wedjat.utility.structure.Drug;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.PillBox;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.DrugSlot;
import tw.nctu.cs.pet.wedjat.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

public class ListPageActivity extends Activity {
	private String TAG = "ListActivity";
	private Context context;
	
	LinearLayout untakenList, takenList;
	TextView title;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_listpage);
		
		context = this;
		
		Intent intent = getIntent();
		int day = intent.getIntExtra("day", -1);
		int slotIndex = intent.getIntExtra("index", -1);

		if (day != -1 && slotIndex != -1) {
			buildList(day, slotIndex);
		} else {
			Log.e(TAG, "value passed by intent are wrong!");
			ScrollView wrapper = (ScrollView) findViewById(R.id.ScrollWrapper);
			wrapper.removeAllViews();
			LayoutInflater inflater = (LayoutInflater) this
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			wrapper.addView(inflater.inflate(R.layout.error, null));
		}
	}

	private void buildList(int day, int index) {
		untakenList = (LinearLayout) findViewById(R.id.untaken_list);
		takenList = (LinearLayout) findViewById(R.id.taken_list);
		title = (TextView) findViewById(R.id.h6);

		PillBox pbox = PillBox.getInstance();

		/** set title **/
		String[] array = getResources().getStringArray(R.array.days_full);
		title.setText(array[day] + "/"
				+ pbox.WEEK.get(day).get(index).startTime
				+ " - "
				+ pbox.WEEK.get(day).get(index).endTime);
		title.setTextSize(30);
		
		
		/** set header of list **/
		TextView header = new TextView(context);
		header.setText("need to take");
		header.setTextSize(16);
		header.setTextColor(context.getResources().getColor(R.color.listpage_sub_header_text));
		header.setBackgroundColor(context.getResources().getColor(R.color.listpage_sub_header));
		header.setGravity(Gravity.CENTER);
		untakenList.addView(header);
		header = new TextView(context);
		header.setText("taken");
		header.setTextSize(16);
		header.setTextColor(context.getResources().getColor(R.color.listpage_sub_header_text));
		header.setBackgroundColor(context.getResources().getColor(R.color.listpage_sub_header));
		header.setGravity(Gravity.CENTER);
		takenList.addView(header);
		
		/** set content of list **/
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if(pbox.WEEK.get(day).get(index) instanceof Slot.DrugSlot){
			Slot.DrugSlot slot = (DrugSlot) pbox.WEEK.get(day).get(index);
			
			for(Drug drug : slot.drugList){
				View listItem = inflater.inflate(R.layout.list_item, null); 
				listItem.setTag(drug.id);
				
				// set pic 
				ImageButton drugPic =(ImageButton) listItem.findViewById(R.id.drugpic);
				drugPic.setOnClickListener(actionOnDrugpic);
				drugPic.setTag(drug.id);
				
				// set drug info
				TextView name = (TextView) listItem.findViewById(R.id.name_row);
				name.setText("name: "+drug.id+" (db index)");
				TextView dosage = (TextView) listItem.findViewById(R.id.dosage_row);
				dosage.setText("dosage: "+drug.dosage);
				
				LinearLayout symbolRow = (LinearLayout) findViewById(R.id.symbol_row);
				
				
				// dispatch
				if(drug.state == Drug.STATE_UNTAKEN){
					untakenList.addView(listItem);
					
				}else if(drug.state == Drug.STATE_TAKEN){
					takenList.addView(listItem);
					
				}else if(drug.state == Drug.STATE_DELAYED){
					untakenList.addView(listItem);
					
				}
			}
			
		}else if(pbox.WEEK.get(day).get(index) instanceof Slot.ActionSlot){
			//TODO: if this slot is actionSlot ....
		}
	}
	
	OnClickListener actionOnDrugpic = new OnClickListener(){

		@Override
		public void onClick(View v) {
			String target = (String) v.getTag();
			
			boolean moved = false;
			
			Log.d(TAG, ""+untakenList.getChildCount());
			
			for(int i=0; i< untakenList.getChildCount(); ++i){
				
				if(target.equals(untakenList.getChildAt(i).getTag())){
					moved = true;
					//swap
					View tmp = untakenList.getChildAt(i);
					untakenList.removeView(tmp);
					takenList.addView(tmp);
				}
			}
			
			if(moved) 
				return;
			
			for(int i=0; i< takenList.getChildCount(); ++i){
				
				if(target.equals(takenList.getChildAt(i).getTag())){
					moved = true;
					//swap
					View tmp = takenList.getChildAt(i);
					takenList.removeView(tmp);
					untakenList.addView(tmp);
				}
			}
		}
	};
}
