package tw.edu.nctu.cs.pet.wedjat.receiver;

import tw.edu.nctu.cs.pet.wedjat.service.simplealarm.AlarmServiceSimple;
import tw.nctu.cs.pet.wedjat.R;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

public class AlarmReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {

		
		//TODO: click the notification will bring user to ListPage
		//check
		//http://developer.android.com/guide/topics/ui/notifiers/notifications.html#ExtendedNotification
		PendingIntent forwordIntent = null;
		
		NotificationCompat.Builder mBuilder =
			    new NotificationCompat.Builder(context)
				.setPriority(NotificationCompat.PRIORITY_HIGH)
			    .setSmallIcon(R.drawable.notification_pic)
			    .setContentTitle("Wedjat")	
			    .setContentText("message")
			    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
			    .setAutoCancel(true)
			    .setVibrate(new long[]{0, 200, 500});
		
		NotificationManager mNotifyMgr = 
		        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		
		int mNotificationId = 001;

		mNotifyMgr.notify(mNotificationId, mBuilder.build());
		
		/* prepare next time slot */
		setNextAlarm(context);
	}
	
	private void setNextAlarm(Context context){
		Intent nextAlarm = new Intent();
		nextAlarm.setAction(AlarmServiceSimple.ACTION_SET_NEXT_ALARM);
		nextAlarm.setClass(context, AlarmServiceSimple.class);
		context.startService(nextAlarm);
	}

}
