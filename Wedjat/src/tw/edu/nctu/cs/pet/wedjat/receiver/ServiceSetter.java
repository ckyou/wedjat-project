package tw.edu.nctu.cs.pet.wedjat.receiver
;

import tw.edu.nctu.cs.pet.wedjat.service.simplealarm.AlarmServiceSimple;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class ServiceSetter extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		
		//setup the alarm
		Intent callUpAlarmServiceSimple = new Intent(context, AlarmServiceSimple.class);
		callUpAlarmServiceSimple.setAction(AlarmServiceSimple.ACTION_SET_NEXT_ALARM);
		context.startService(callUpAlarmServiceSimple);
	}

}
