package tw.edu.nctu.cs.pet.wedjat.service.alarm;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

public class AlarmProvider extends ContentProvider {

	private static final UriMatcher mUriMatcher;
	
	private static final String AUTHORITY = "nctu.cs.pet.wedjat";
	private static final String DATABASE_TABLE = "alarm";
	private static final int ALARMS = 1;
	
	static {
	        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
	        mUriMatcher.addURI(AUTHORITY, DATABASE_TABLE, ALARMS);

	}
	
	
	
	
	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean onCreate() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

}
