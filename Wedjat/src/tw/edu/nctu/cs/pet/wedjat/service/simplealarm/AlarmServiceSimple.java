package tw.edu.nctu.cs.pet.wedjat.service.simplealarm;

import java.util.ArrayList;
import java.util.Calendar;

import org.joda.time.DateTime;
import org.joda.time.Duration;

import tw.edu.nctu.cs.pet.wedjat.page.entrance.ScheduleReader;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.PillBox;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.ActionSlot;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.BaseSlot;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.DrugSlot;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

/**
 * AlarmServiceSimple is a service to arrange all alarm event
 * 
 * @author CKYOU
 */
public class AlarmServiceSimple extends IntentService {
	private String TAG = "AlarmService-S";

	/*
	 * these actions are for AlarmReceiver Don't forget to add these in Manifest
	 * as static intent-filter regist
	 */
	public static String ACTION_NOTIFY_TAKE = "wedjat.action.notify.take.drug";
	public static String ACTION_NOTIFY_KEEP = "wedjat.action.notify.keep.status";

	public static String ACTION_SET_NEXT_ALARM = "wedjat.action.set.next";
	public static String ACTION_SET_DELAY = "wedjat.action.set.delay";
	public static String ACTION_TEST_NOTIFICATION = "wedjat.action.test.notification";

	private static int requestCode = 10;

	public AlarmServiceSimple() {
		super("Wedjat AlarmService-S");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		if (intent.getAction().equals(ACTION_SET_NEXT_ALARM)) {
			Log.i(TAG, "set next alarm");

			PillBox pbox = PillBox.getInstance();

			Calendar c = Calendar.getInstance();
			int today = c.get(Calendar.DAY_OF_WEEK) - 1;
			int hourNow = c.get(Calendar.HOUR_OF_DAY);

			ArrayList<BaseSlot> slotList = pbox.WEEK.get(today);

			boolean founded = false;
			for (int i = 0; i < slotList.size(); ++i) {
				if (hourNow < slotList.get(i).startTime) {
					founded = true;
					// this slot is next slot, then set notification
					setAlarmEvent(today, i, slotList.get(i));
					break;
				}
			}

			if (!founded) {
				// set next day's first slot as the next alarm
				setAlarmEvent(today + 1, 0, pbox.WEEK.get(today + 1).get(0));
			}

			Log.i(TAG, "finish set next alarm !");
		} else if (intent.getAction().equals(ACTION_SET_DELAY)) {

		} else if (intent.getAction().equals(ACTION_TEST_NOTIFICATION)) {
			Intent intentForward = new Intent();
			intentForward.setAction(ACTION_NOTIFY_TAKE);
			intentForward.putExtra("day", 0);
			intentForward.putExtra("index", 0);
			this.sendBroadcast(intentForward);
		}
	}

	private void setAlarmEvent(int day, int index, BaseSlot slot) {

		Intent intentForward = new Intent();
		intentForward.putExtra("day", day);
		intentForward.putExtra("index", index);
		PendingIntent pi = null;

		if (slot instanceof DrugSlot) {
			Log.i(TAG, "next alarm is drug-take-alarm");

			intentForward.setAction(ACTION_NOTIFY_TAKE);
			pi = PendingIntent.getBroadcast(this, requestCode, intentForward,
					PendingIntent.FLAG_UPDATE_CURRENT);

		} else if (slot instanceof ActionSlot) {
			Log.i(TAG, "next alarm is status-keep-alarm");

			intentForward.setAction(ACTION_NOTIFY_KEEP);
			pi = PendingIntent.getBroadcast(this, requestCode, intentForward,
					PendingIntent.FLAG_UPDATE_CURRENT);

		} else {
			// ERROR happened
			return;
		}

		/** Calculate trigger time **/
		// this part uses Joma time library
		DateTime start = new DateTime();
		DateTime end;
		if(slot.startTime > start.getHourOfDay()){
			//means today
			end = new DateTime(start.getYear(), start.getMonthOfYear(),start.getDayOfMonth(), 
									slot.startTime, 0, 0);
		}else{
			end = new DateTime(start.getYear(), start.getMonthOfYear(),start.getDayOfMonth(), 
					slot.startTime, 0, 0);
			end = end.plusDays(1);
		}
		Duration dur = new Duration(start, end);
		
		Log.i(TAG, "trigger millisecond: "+dur.getMillis());
		AlarmManager am = (AlarmManager) getSystemService(this.ALARM_SERVICE);
		am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+dur.getMillis(), pi);
	}
}
