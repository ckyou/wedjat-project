package tw.edu.nctu.cs.pet.wedjat.test;

import org.joda.time.DateTime;

import tw.edu.nctu.cs.pet.wedjat.service.simplealarm.AlarmServiceSimple;
import tw.nctu.cs.pet.wedjat.R;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class TestAlarmServiceActivity extends Activity {
	private final String TAG = "TEST-BoxActivity";
	private Context context;

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout._empty_layout);

		context = this;
		LinearLayout a = (LinearLayout) findViewById(R.id.LinearLayout1);

		Button bt = new Button(this);
		bt.setText("Send a notification");
		bt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent service = new Intent(context, AlarmServiceSimple.class);
				service.setAction(AlarmServiceSimple.ACTION_TEST_NOTIFICATION);
				startService(service);
			}
		});
		a.addView(bt);
	}
}
