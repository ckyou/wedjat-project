package tw.edu.nctu.cs.pet.wedjat.test;

import tw.edu.nctu.cs.pet.wedjat.page.boxpage.ScrollableTableView;
import tw.nctu.cs.pet.wedjat.R;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.Button;
import android.widget.LinearLayout;

public class TestBoxActivity extends Activity{
	private final String TAG = "TEST-BoxActivity";
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout._empty_layout);
		
		String[] array = getResources().getStringArray(R.array.days_abbreviation);
		for (String i : array) {
	        Log.d(TAG, i);
	    }
		array = getResources().getStringArray(R.array.days_full);
		for (String i : array) {
	        Log.d(TAG, i);
	    }
		
		LinearLayout a = (LinearLayout) findViewById(R.id.LinearLayout1);
		ScrollableTableView b = new ScrollableTableView(this);
		LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		b.addContent((LinearLayout)inflater.inflate(R.layout._test_data, null));
		b.addHeader(inflater.inflate(R.layout._test_header, null));
		a.addView(b);
		
		Button bt = (Button) findViewById(R.id.Button20);

	}
}
