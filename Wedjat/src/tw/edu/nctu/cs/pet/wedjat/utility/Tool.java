package tw.edu.nctu.cs.pet.wedjat.utility;

import java.io.File;
import java.util.Calendar;

import android.content.Context;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.WindowManager;


public class Tool {
	/** size parameter**/
	public static int screenWidth, screenHeight;
	
	public static void calcScreenSize(Context context){
		DisplayMetrics dm = new DisplayMetrics();
		WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
		wm.getDefaultDisplay().getMetrics(dm);
		Tool.screenWidth = dm.widthPixels;
		Tool.screenHeight = dm.heightPixels;
	}
	
	public static boolean schuduleIsExist(){
		File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath()
				+ Config.scheduleFolder + Config.scheduleName);
		return file.exists();
	}
	
	public static int getToday(){
		Calendar c = Calendar.getInstance(); 
		return c.get(Calendar.DAY_OF_WEEK) - 1;
	}
	
	public static int getHourNow(){
		Calendar c = Calendar.getInstance(); 
		return c.get(Calendar.HOUR_OF_DAY);
	}
}
