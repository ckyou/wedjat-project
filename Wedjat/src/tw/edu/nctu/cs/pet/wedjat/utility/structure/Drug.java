package tw.edu.nctu.cs.pet.wedjat.utility.structure;

public class Drug {
	public static final int STATE_TAKEN = 0;
	public static final int STATE_UNTAKEN = 1;
	public static final int STATE_DELAYED = 2;
	
	public String id;
	public String dosage;
	public int state;
	
	public Drug(String drugId, String dosage) {
		this.id = drugId;
		this.dosage = dosage;
		
		//assign default values
		state = STATE_UNTAKEN;
	}
	
	public Drug(String drugId, String dosage, int state) {
		this.id = drugId;
		this.dosage = dosage;
		this.state = state;
	}
}
