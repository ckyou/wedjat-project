package tw.edu.nctu.cs.pet.wedjat.utility.structure;

import java.util.ArrayList;

import tw.edu.nctu.cs.pet.wedjat.page.entrance.ScheduleReader;
import tw.edu.nctu.cs.pet.wedjat.utility.Config;
import tw.edu.nctu.cs.pet.wedjat.utility.structure.Slot.BaseSlot;

public class PillBox {
	public static final int MONDAY = 1;
	public static final int TUESDAY = 2;
	public static final int WEDNESDAY = 3;
	public static final int THRSDAY = 4;
	public static final int FRIDAY = 5;
	public static final int SATURDAY = 6;
	public static final int SUNDAY = 0;
		
	private static PillBox pbox = null; 
	public ArrayList<BaseSlot> ColMON, ColTUE, ColWED, ColTHR, ColFRI, ColSAT, ColSUN;
	public ArrayList<ArrayList<BaseSlot>> WEEK;
	
	private PillBox() {
		ColMON= new ArrayList<BaseSlot>();
		ColTUE= new ArrayList<BaseSlot>();
		ColWED= new ArrayList<BaseSlot>();
		ColTHR= new ArrayList<BaseSlot>();
		ColFRI= new ArrayList<BaseSlot>();
		ColSAT= new ArrayList<BaseSlot>();
		ColSUN= new ArrayList<BaseSlot>();
		
		WEEK = new ArrayList<ArrayList<BaseSlot>>(7);
		WEEK.add(ColSUN);
		WEEK.add(ColMON);
		WEEK.add(ColTUE);
		WEEK.add(ColWED);
		WEEK.add(ColTHR);
		WEEK.add(ColFRI);
		WEEK.add(ColSAT);
	}
	
	/**
	 * Get PillBox instance which is parsed from proto file
	 * <br> This also means that this method will always parse from file once call it
	 * @return PillBox instance
	 */
	synchronized public static PillBox getInstance(){
		boolean result = false;
		if(pbox == null){
			 pbox = new PillBox();
		}
		
		/** Read the schedule**/
		if(Config._USE_TEST_SCHEDULE_WITHOUT_GBP){
			result = ScheduleReader.fromDebugSchedule();
		}else{
			result = ScheduleReader.fromProtobufFile();
		}
		
		if(result)
			return pbox;
		else
			return null;
	}
	
	/**
	 * Get empty PillBox instance 
	 * @return
	 */
	synchronized public static PillBox getNewInstance(){
		if(pbox == null){
			 pbox = new PillBox();
		}else{
			pbox.clear();
		}
		return pbox;
	}
	
	synchronized public void clear(){
		for(int i=0; i< pbox.WEEK.size(); ++i){
			pbox.WEEK.get(i).clear();
		}
	}
}
