package tw.edu.nctu.cs.pet.wedjat.utility.structure;

import java.util.ArrayList;

public class Slot {
	
	public static abstract class BaseSlot{
		public int startTime, endTime;
	}
	
	public static class DrugSlot extends BaseSlot{
		public ArrayList<Drug> drugList;
		
		public DrugSlot(int startTime, int endTime, ArrayList<Drug> drugList) {
			this.startTime = startTime;
			this.endTime = endTime;
			this.drugList = drugList;
		}
	}
	
	public static class ActionSlot extends BaseSlot{
		public final int DONT_DO_THAT = 0;
		public final int KEEP_STOMACH_EMPTY = 1;
		public final int TAKE_MEALS = 2;
		
		public int type;
		public String extraInfo;
		
		public ActionSlot(int startTime, int endTime, int type) {
			this.startTime = startTime;
			this.endTime = endTime;
			this.type = type;
		}
		
		public ActionSlot(int startTime, int endTime, int type, String extraInfo) {
			this.startTime = startTime;
			this.endTime = endTime;
			this.type = type;
			this.extraInfo = extraInfo;
		}
	}
	
	
}
